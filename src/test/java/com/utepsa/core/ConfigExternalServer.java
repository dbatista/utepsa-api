package com.utepsa.core;

import com.utepsa.config.ExternalServer;

/**
 * Created by David-SW on 26/04/2017.
 */
public class ConfigExternalServer {
    public static ExternalServer getExternalServer(){
        ExternalServer externalServer = new ExternalServer();
        externalServer.setName("Utepsa");
        externalServer.setUrl("http://190.186.43.203/api/");

        return externalServer;
    }
}
