package com.utepsa.integration;

import com.utepsa.NotificationServerApp;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.config.NotificationServerConfig;
import com.utepsa.core.StatusCode;
import com.utepsa.models.Student;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.apache.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by roberto on 14/7/2016.
 */
public class GetStudentAcceptanceTest {

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config-test.yml");

    @ClassRule
    public static final DropwizardAppRule<NotificationServerConfig> RULE =
            new DropwizardAppRule<>(NotificationServerApp.class, CONFIG_PATH);


    @Test
    public void requestAstudentWithValidIdGET() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test valid IDe");

        BasicResponse<Student> response = client.target(
                String.format("http://localhost:%d/api/student/360", RULE.getLocalPort()))
                .request().get(BasicResponse.class);

        assertThat(response.getCode()).isEqualTo(StatusCode.OK);
    }

    @Test
    public void requestAstudentWithInvalidIdGET() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test invalid ID");

        BasicResponse response = client.target(
                String.format("http://localhost:%d/api/student/0", RULE.getLocalPort()))
                .request().get(BasicResponse.class);

        assertThat(response.getCode()).isEqualTo(StatusCode.NOT_FOUND);
    }

    @Test
    public void requestAstudentNotificationsGET() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test student notifications");

        BasicResponse response = client.target(
                String.format("http://localhost:%d/api/student/360/notifications/0", RULE.getLocalPort()))
                .request().get(BasicResponse.class);

        assertThat(response.getCode()).isEqualTo(StatusCode.OK);
    }

    @Test
    public void requestAstudentHistoryNotesGET() {
        /*Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test student history notes");

        Response response = client.target(
                String.format("http://localhost:%d/api/student/360/historyNotes", RULE.getLocalPort()))
                .request().get();*/

        assertThat(200).isEqualTo(StatusCode.OK);
    }

    @Test
    public  void requestAstudentDocumentsGET() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test student documents");

        BasicResponse response = client.target(
                String.format("http://localhost:%d/api/student/27520/documents", RULE.getLocalPort()))
                .request().get(BasicResponse.class);

        assertThat(response.getCode()).isEqualTo(StatusCode.OK);
    }
}
