package com.utepsa.integration;

import com.utepsa.NotificationServerApp;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.config.NotificationServerConfig;
import com.utepsa.core.StatusCode;
import com.utepsa.models.Career;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by David on 08/11/2016.
 */
public class GetCareersAcceptanceTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config-test.yml");

    @ClassRule
    public static final DropwizardAppRule<NotificationServerConfig> RULE =
            new DropwizardAppRule<>(NotificationServerApp.class, CONFIG_PATH);

    @Before
    public void setup(){

    }

    @Test
    public void requestCarrersUp() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Existing Careers");

        BasicResponse<Career> response = client.target(
                    String.format("http://localhost:%d/api/careers/", RULE.getLocalPort()))
                    .request().get(BasicResponse.class);

        assertThat(response.getCode()).isEqualTo(StatusCode.OK);
    }

    @Test
    public void requestCarrersByPreGradeUp() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Not Existing Career by Pre Grade");

        BasicResponse<Career> response = client.target(
                String.format("http://localhost:%d/api/careers/grade/PRE", RULE.getLocalPort()))
                .request().get(BasicResponse.class);

        assertThat(response.getCode()).isEqualTo(StatusCode.OK);
    }

    @Test
    public void requestCarrersByPostGradeUp() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Not Existing Career by Post Grade");

        BasicResponse<Career> response = client.target(
                String.format("http://localhost:%d/api/careers/grade/POST", RULE.getLocalPort()))
                .request().get(BasicResponse.class);

        assertThat(response.getCode()).isEqualTo(StatusCode.OK);
    }

    @Test
    public void badRequestCarrersByGradeUp() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Bad param grade");

        BasicResponse<Career> response = client.target(
                String.format("http://localhost:%d/api/careers/grade/asd", RULE.getLocalPort()))
                .request().get(BasicResponse.class);

        assertThat(response.getCode()).isEqualTo(StatusCode.NOT_FOUND);
    }
}
