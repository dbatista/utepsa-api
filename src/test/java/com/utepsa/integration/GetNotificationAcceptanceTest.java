package com.utepsa.integration;

import com.utepsa.NotificationServerApp;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.config.NotificationServerConfig;
import com.utepsa.core.StatusCode;
import com.utepsa.core.TypeAudience;
import com.utepsa.models.CategoryNotification;
import com.utepsa.models.Notification;
import com.utepsa.models.NotificationAudience;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import io.dropwizard.util.Duration;
import org.apache.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import java.util.Collections;
import java.util.Date;
import java.util.IdentityHashMap;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by David on 26/07/2016.
 */
public class GetNotificationAcceptanceTest {
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("config-test.yml");

    @ClassRule
    public static final DropwizardAppRule<NotificationServerConfig> RULE =
            new DropwizardAppRule<>(NotificationServerApp.class, CONFIG_PATH);

    @Test
    public void requestStudentsNotificacionsByRegisterCode() {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("Test valid Register Code in Student Notification");

        Response response = client.target(
                String.format("http://localhost:%d/api/notifications/1", RULE.getLocalPort()))
                .request().get();

        assertThat(response.getStatus()).isEqualTo(StatusCode.OK);
    }

    @Test
    public void testCreateAnotificationsWithPOST() throws Exception {
        Set<NotificationAudience> audience = Collections.newSetFromMap(new IdentityHashMap<NotificationAudience,Boolean>());

        audience.add(new NotificationAudience("Public", new com.utepsa.models.TypeAudience(1, "PUBLIC")));


        Notification notification = new Notification(0, new Date().getTime(), "test", "test",new CategoryNotification(1, "Notificacion", null), audience);

        JerseyClientConfiguration config = new JerseyClientConfiguration();
        config.setTimeout(Duration.seconds(20));

        Client client = new JerseyClientBuilder(RULE.getEnvironment()).using(config).build("test create notification");


        BasicResponse response = client
                .target(String.format("http://localhost:%d/api/notifications", RULE.getLocalPort()))
                .request()
                .post(Entity.json(notification)).readEntity(BasicResponse.class);
        assertThat(response.getCode()).isEqualTo(StatusCode.CREATED);
    }
}
