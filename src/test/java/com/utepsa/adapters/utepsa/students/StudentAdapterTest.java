package com.utepsa.adapters.utepsa.students;

import com.utepsa.config.ExternalServer;
import com.utepsa.core.ConfigExternalServer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Created by david on 4/10/16.
 */
public class StudentAdapterTest {
    private StudentAdapter studentAdapter;

    @Before
    public void SetUp() {
        this.studentAdapter = new StudentAdapter(ConfigExternalServer.getExternalServer());
    }

    @Test
    public void testIsExternalServerUP() throws IOException {
        Assert.assertTrue(this.studentAdapter.isServerUp());
    }

    @Test
    public void testStudentData() throws IOException {
        StudentData listStudents = this.studentAdapter.getStudent("0000000018");
        Assert.assertNotNull(listStudents);
    }
}
