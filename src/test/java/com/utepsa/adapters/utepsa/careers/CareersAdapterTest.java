package com.utepsa.adapters.utepsa.careers;

import com.utepsa.config.ExternalServer;
import com.utepsa.core.ConfigExternalServer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Created by David on 09/11/2016.
 */
public class CareersAdapterTest {
    private CareersAdapter adapter;

    @Before
    public void SetUp() {
        this.adapter = new CareersAdapter(ConfigExternalServer.getExternalServer());
    }

    @Test
    public void testIsExternalServerUP() throws IOException {
        Assert.assertTrue(this.adapter.isServerUp());
    }

    @Test
    public void testGetAllCareers() throws IOException {
        List<CareersDATA> listDocumentsStudent = this.adapter.getAllCareers();
        Assert.assertNotNull(listDocumentsStudent);
    }

    @Test
    public void testGetACareerByCareerCode() throws IOException {
        CareersDATA career = this.adapter.getCareersByCareerCode("010");
        career.setCrr_descripcion(career.getCrr_descripcion().trim());
        career.setSca_descripcion(career.getSca_descripcion().trim());
        Assert.assertEquals(career.toString(), new CareersDATA("010","AUDITORIA FINANCIERA", "03", "FACULTAD DE CIENCIAS EMPRESARIALES").toString());
    }
}
