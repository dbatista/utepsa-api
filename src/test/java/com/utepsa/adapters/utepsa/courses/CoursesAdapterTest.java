package com.utepsa.adapters.utepsa.courses;

import com.utepsa.config.ExternalServer;
import com.utepsa.core.ConfigExternalServer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Created by shigeots on 21-11-16.
 */
public class CoursesAdapterTest {
    private CoursesAdapter adapter;

    @Before
    public void SetUp() {
        this.adapter = new CoursesAdapter(ConfigExternalServer.getExternalServer());
    }

    @Test
    public void testIsExternalServerUP() throws IOException {
        Assert.assertTrue(this.adapter.isServerUp());
    }

    @Test
    public void testGetAllCourses() throws IOException {
        List<CoursesDATA> listCourses = this.adapter.getAllCourses();
        Assert.assertNotNull(listCourses);
    }
}
