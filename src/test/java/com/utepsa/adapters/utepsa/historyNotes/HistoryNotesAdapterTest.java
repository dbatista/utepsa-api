package com.utepsa.adapters.utepsa.historyNotes;


import com.utepsa.config.ExternalServer;
import com.utepsa.core.ConfigExternalServer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Created by David on 11/10/2016.
 */
public class HistoryNotesAdapterTest {
    private HistoryNotesAdapter historyNotesAdapter;

    @Before
    public void SetUp() {
        this.historyNotesAdapter = new HistoryNotesAdapter(ConfigExternalServer.getExternalServer());
    }

    @Test
    public void testIsExternalServerUP() throws IOException {
        Assert.assertTrue(this.historyNotesAdapter.isServerUp());
    }

    @Test
    public void testGetAllHistoryNotesByStudent() throws IOException {
        List<HistoryNotesData> listHistoryNotes = this.historyNotesAdapter.getAllHistoryNotesStudent("0000000018");
        Assert.assertNotNull(listHistoryNotes);
    }
}
