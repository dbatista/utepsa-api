package com.utepsa.adapters.gcm;

import com.utepsa.config.ExternalServer;
import org.apache.http.HttpStatus;
import org.glassfish.grizzly.utils.ArraySet;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by roberto on 25/8/2016.
 */
public class GCMAdapterTest {

    private GCMAdapter adapter;
    private Set<String> ids;

    @Before
    public void SetUp() {
        ExternalServer server = new ExternalServer();
        server.setName("GCM");
        server.setUrl("https://gcm-http.googleapis.com/gcm/send");
        server.setKey("AIzaSyCEoLg6jafbq1NRV0vFVnP5y3HlKLPyB28");

        // Just for test using Alexis DeviceID
        String[] idsList = {
                "nf4KeO4VU3A:APA91bF8F_p5WIFySeLK0lsyNAsbbmS-Hk-1Buc5sFci9fViYut2nIZAk_E60G20upSxI9inpe3GoRNHM0lhFyWtfkBmr5y0yD6fnvGGS7aF8_X2bG7wclqlTHGcrcr7FgX1wXaQcOc0"   };

        this.ids = new HashSet<>(Arrays.asList(idsList));

        this.adapter = new GCMAdapter(server);
    }

    @Test
    public void sendSimpleNotification() throws Exception {
        NotificationGCM notification = new NotificationGCM();
        notification.setNotification(new Data("Test", "This is a test notification"));
        notification.setRegistration_ids(this.ids);

        assertEquals(HttpStatus.SC_OK, this.adapter.sendNotification(notification));
    }
}
