package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Course;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by JoseCarlos on 27-07-16.
 */
public class CourseTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Course course;

    @Before
    public void setup() throws Exception {
        this.course = new Course(1, "00003", "ADMINISTRACION 140", "AAD-140");
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/course.json"), Course.class));

        assertThat(MAPPER.writeValueAsString(course)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        Course course = MAPPER.readValue(fixture("fixtures/course.json"), Course.class);
        assertThat(course)
                .isEqualTo(this.course);
    }
}
