package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Career;
import com.utepsa.models.Course;
import com.utepsa.models.Faculty;
import com.utepsa.models.HistoryNotes;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by DEVOPS on 18/04/2017.
 */
public class HistoryNotesTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private HistoryNotes historyNotes;

    @Before
    public void setup() throws Exception {
        Course course = new Course(1, "00003", "ADMINISTRACION 140", "AAD-140");
        this.historyNotes = new HistoryNotes(1,10,course, 51,0,"Primero","Primero");
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/historyNotes.json"), HistoryNotes.class));

        assertThat(MAPPER.writeValueAsString(historyNotes))
                .isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/historyNotes.json"), HistoryNotes.class))
                .isEqualTo(this.historyNotes);
    }
}
