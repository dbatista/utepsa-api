package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Permission;
import com.utepsa.models.PermissionCredential;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by DEVOPS on 19/04/2017.
 */
public class PermissionCredentialTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private PermissionCredential permissionCredential;

    @Before
    public void setup() throws Exception {
        Permission permission = new Permission(1,"","");
        this.permissionCredential = new PermissionCredential(1,10,permission,true);
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/permissionCredential.json"), PermissionCredential.class));

        assertThat(MAPPER.writeValueAsString(permissionCredential))
                .isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/permissionCredential.json"), PermissionCredential.class))
                .isEqualTo(this.permissionCredential);
    }
}
