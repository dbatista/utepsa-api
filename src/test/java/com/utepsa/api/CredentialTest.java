package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.*;
import io.dropwizard.jackson.Jackson;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by DEVOPS on 18/04/2017.
 */
public class CredentialTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Credential credential;
    @Before
    public void setup() throws Exception {
        DateTimeFormatter format = ISODateTimeFormat.dateHourMinuteSecondMillis();
        DateTime time = format.parseDateTime("1998-02-05T01:00:00.00");
        Date date = time.toDate();
        Faculty faculty = new Faculty(1, "Facultad de tecnología", "PREGRADO", "FECT");
        Career career = new Career(1, "108", "Sistemas", 1, faculty);
        Student student = new Student(1,"Armando", "Carpa", "Roja",date, "Masculino", "acarpa.est.@utepsa.edu","acarpa2.est.@utepsa.edu", "75566421", "76532148", "0000376520", "376520", "CI", "001", 1, career);
        this.credential = new Credential (1, "Juan","12345678", "12345","123","10:53", true,new Role(1,"STUDENT"),student,true);
    }

    @Test
    public void testSerializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/credential.json"), Credential.class));
        assertThat(MAPPER.writeValueAsString(credential))
                .isEqualTo(expected);
    }

    @Test
    public void testDeserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/credential.json"), Credential.class))
                .isEqualTo(this.credential);
    }
}
