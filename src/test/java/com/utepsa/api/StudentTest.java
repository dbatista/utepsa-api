package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Career;
import com.utepsa.models.Faculty;
import com.utepsa.models.Student;
import io.dropwizard.jackson.Jackson;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static io.dropwizard.testing.FixtureHelpers.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by roberto on 14/7/2016.
 */
public class StudentTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Student student;

    @Before
    public void setUp() throws Exception {
        DateTimeFormatter format = ISODateTimeFormat.dateHourMinuteSecondMillis();
        DateTime time = format.parseDateTime("1998-02-05T01:00:00.00");
        Date date = time.toDate();
        Faculty faculty=new Faculty(1, "Facultad de tecnología", "PREGRADO", "FECT");
        Career career=new Career(1, "108", "Sistemas", 1, faculty);
        this.student = new Student(1,"Armando", "Carpa", "Roja", date, "Masculino", "acarpa.est.@utepsa.edu","acarpa2.est.@utepsa.edu", "75566421", "76532148", "0000376520", "376520", "CI", "001", 1, career);
    }
    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/student.json"), Student.class));
        assertThat(MAPPER.writeValueAsString(student)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/student.json"),
                Student.class)).isEqualTo(student);
    }
}
