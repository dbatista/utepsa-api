package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Career;
import com.utepsa.models.CurrentSemester;
import com.utepsa.models.Faculty;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by DEVOPS on 18/04/2017.
 */
public class currentSemesterTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private CurrentSemester currensemester;

    @Before
    public void setup() throws Exception {
        this.currensemester = new CurrentSemester(1,"Primer", 1,true);
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/currentSemester.json"), CurrentSemester.class));

        assertThat(MAPPER.writeValueAsString(currensemester))
                .isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/currentSemester.json"), CurrentSemester.class))
                .isEqualTo(this.currensemester);
    }
}
