package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Faculty;
import com.utepsa.models.Permission;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by DEVOPS on 19/04/2017.
 */
public class PermissionTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Permission permission;

    @Before
    public void setup() throws Exception {
        this.permission = new Permission(1,"","");
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/permission.json"), Permission.class));

        assertThat(MAPPER.writeValueAsString(permission))
                .isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/permission.json"), Permission.class))
                .isEqualTo(this.permission);
    }
}
