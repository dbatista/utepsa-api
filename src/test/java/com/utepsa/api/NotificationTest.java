package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.CategoryNotification;
import com.utepsa.models.Notification;
import com.utepsa.models.NotificationAudience;
import com.utepsa.models.TypeAudience;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Date;
import java.util.IdentityHashMap;
import java.util.Set;


import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by roberto on 19/7/2016.
 */
public class NotificationTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Notification notification ;

    @Before
    public void setup() {
        Set<NotificationAudience> audience=Collections.newSetFromMap(new IdentityHashMap<NotificationAudience, Boolean>());
        audience.add(new NotificationAudience("public", new TypeAudience(com.utepsa.core.TypeAudience.PUBLIC,"student")));
        CategoryNotification categorynotification= new CategoryNotification(1,"Noticias", null);
        this.notification = new Notification(1, 00001, "Feria Jets 2016", "Les invitamos a...", categorynotification, audience);
        notification.setId(1);
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/notification.json"), Notification.class));
        assertThat(MAPPER.writeValueAsString(notification)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/notification.json"),
                Notification.class)).isEqualTo(notification);
    }
}