package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.CategoryNotification;
import com.utepsa.models.Faculty;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by DEVOPS on 18/04/2017.
 */
public class CategoryNotificationTest {
        private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

        private CategoryNotification categoryNotification;
        @Before
        public void setup() throws Exception {
        this.categoryNotification = new CategoryNotification (1, "Noticias", null);
    }

        @Test
        public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/categoryNotification.json"), CategoryNotification.class));

        assertThat(MAPPER.writeValueAsString(categoryNotification))
                .isEqualTo(expected);
    }

        @Test
        public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/categoryNotification.json"), CategoryNotification.class))
                .isEqualTo(this.categoryNotification);
    }
}
