package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Permission;
import com.utepsa.models.PermissionRole;
import com.utepsa.models.Role;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by DEVOPS on 19/04/2017.
 */
public class PermissionRoleTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private PermissionRole permissionRole;

    @Before
    public void setup() throws Exception {
        Role role=new Role(1,"");
        Permission permission = new Permission(1,"","");
        this.permissionRole = new PermissionRole(1,role,permission,true);
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/permissionRole.json"), PermissionRole.class));

        assertThat(MAPPER.writeValueAsString(permissionRole))
                .isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/permissionRole.json"), PermissionRole.class))
                .isEqualTo(this.permissionRole);
    }
}
