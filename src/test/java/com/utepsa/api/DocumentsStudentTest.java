package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Document;
import com.utepsa.models.DocumentStudent;
import io.dropwizard.jackson.Jackson;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by shigeots on 31-10-16.
 */
public class DocumentsStudentTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private  DocumentStudent documentsStudent;

    @Before
    public void setUp() throws  Exception {
        Document document= new Document(1, "001", "Titulo de Bachiller", 1);
        DateTimeFormatter format = ISODateTimeFormat.dateHourMinuteSecondMillis();
        DateTime time = format.parseDateTime("1998-02-05T01:00:00.00");
        Date date = time.toDate();
        this.documentsStudent = new DocumentStudent(1, 10,document, date, null, "O",true);
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/documentsStudent.json"), DocumentStudent.class));

        assertThat(MAPPER.writeValueAsString(documentsStudent)).isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/documentsStudent.json"),
                DocumentStudent.class)).isEqualTo(documentsStudent);
    }
}
