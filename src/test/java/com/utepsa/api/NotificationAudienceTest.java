package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Course;
import com.utepsa.models.HistoryNotes;
import com.utepsa.models.NotificationAudience;
import com.utepsa.models.TypeAudience;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by DEVOPS on 19/04/2017.
 */
public class NotificationAudienceTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private NotificationAudience notificationAudience;

    @Before
    public void setup() throws Exception {
        TypeAudience typeaudience=new TypeAudience(1,"General");
        this.notificationAudience = new NotificationAudience("",typeaudience);
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/notificationAudience.json"), NotificationAudience.class));

        assertThat(MAPPER.writeValueAsString(notificationAudience))
                .isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/notificationAudience.json"), NotificationAudience.class))
                .isEqualTo(this.notificationAudience);
    }
}
