package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Document;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by David-SW on 17/04/2017.
 */
public class DocumentTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Document document;

    @Before
    public void setup() throws Exception {
        this.document = new Document(1, "001", "Titulo de Bachiller", 1);
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/document.json"), Document.class));

        assertThat(MAPPER.writeValueAsString(this.document))
                .isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/document.json"), Document.class))
                .isEqualTo(this.document);
    }
}
