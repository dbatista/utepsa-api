package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Career;
import com.utepsa.models.Faculty;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by DEVOPS on 18/04/2017.
 */
public class FacultyTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Faculty faculty;

    @Before
    public void setup() throws Exception {
        this.faculty = new Faculty(1, "Facultad de tecnología", "PREGRADO", "FECT");
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/faculty.json"), Faculty.class));

        assertThat(MAPPER.writeValueAsString(faculty))
                .isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/faculty.json"), Faculty.class))
                .isEqualTo(this.faculty);
    }
}
