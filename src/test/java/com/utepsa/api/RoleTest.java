package com.utepsa.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.models.Permission;
import com.utepsa.models.Role;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by DEVOPS on 19/04/2017.
 */
public class RoleTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    private Role role;

    @Before
    public void setup() throws Exception {
        this.role = new Role(1,"");
    }

    @Test
    public void serializesToJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/role.json"), Role.class));

        assertThat(MAPPER.writeValueAsString(role))
                .isEqualTo(expected);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/role.json"), Role.class))
                .isEqualTo(this.role);
    }
}
