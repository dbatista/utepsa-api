package com.utepsa.modules;

import com.utepsa.api.views.ViewGcmStudent;
import com.utepsa.config.NotificationServerConfig;
import com.utepsa.models.*;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;

/**
 * Created by David-SW on 08/05/2017.
 */
public class HbnBundle extends HibernateBundle<NotificationServerConfig> {

    public HbnBundle() {
        super(Career.class, Course.class, CareerCourse.class, Faculty.class, NotificationAudience.class,
                Notification.class, CategoryNotification.class, TypeAudience.class, Student.class, HistoryNotes.class,
                Document.class, DocumentStudent.class, Credential.class, Role.class,
                PermissionCredential.class, PermissionRole.class, Permission.class, CurrentSemester.class, ViewGcmStudent.class);
    }

    @Override
    public PooledDataSourceFactory getDataSourceFactory(NotificationServerConfig configuration) {
        return configuration.getDataSourceFactory();
    }
}
