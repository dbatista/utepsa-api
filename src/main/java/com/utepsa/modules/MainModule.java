package com.utepsa.modules;

import com.utepsa.adapters.gcm.GCMAdapter;
import com.utepsa.adapters.utepsa.careers.CareersAdapter;
import com.utepsa.adapters.utepsa.courses.CoursesAdapter;
import com.utepsa.adapters.utepsa.coursesCareer.CoursesCareerAdapter;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentAdapter;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesAdapter;
import com.utepsa.adapters.utepsa.studentCourseRegister.StudentCourseRegisterAdapter;
import com.utepsa.adapters.utepsa.students.StudentAdapter;
import com.utepsa.config.NotificationServerConfig;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

/**
 * Created by David-SW on 08/05/2017.
 */
public class MainModule extends DropwizardAwareModule<NotificationServerConfig> {


    @Override
    protected void configure() {

        bind(HistoryNotesAdapter.class).toInstance(new HistoryNotesAdapter(configuration().getExternalSever("utepsa")));
        bind(CareersAdapter.class).toInstance(new CareersAdapter(configuration().getExternalSever("utepsa")));
        bind(CoursesAdapter.class).toInstance(new CoursesAdapter(configuration().getExternalSever("utepsa")));
        bind(CoursesCareerAdapter.class).toInstance(new CoursesCareerAdapter(configuration().getExternalSever("utepsa")));
        bind(DocumentsStudentAdapter.class).toInstance(new DocumentsStudentAdapter(configuration().getExternalSever("utepsa")));
        bind(StudentCourseRegisterAdapter.class).toInstance(new StudentCourseRegisterAdapter(configuration().getExternalSever("utepsa")));
        bind(StudentAdapter.class).toInstance(new StudentAdapter(configuration().getExternalSever("utepsa")));
        bind(GCMAdapter.class).toInstance(new GCMAdapter(configuration().getExternalSever("GCM")));
    }
}
