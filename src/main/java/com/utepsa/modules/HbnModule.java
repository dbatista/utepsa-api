package com.utepsa.modules;

import com.google.inject.AbstractModule;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.career.CareerRealDAO;
import com.utepsa.db.careerCourse.CareerCourseDAO;
import com.utepsa.db.careerCourse.CareerCourseRealDAO;
import com.utepsa.db.categoryNotification.CategoryNotificationDAO;
import com.utepsa.db.categoryNotification.CategoryNotificationRealDAO;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.course.CourseRealDAO;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.db.credential.CredentialRealDAO;
import com.utepsa.db.currentSemester.CurrentSemesterDAO;
import com.utepsa.db.currentSemester.CurrentSemesterRealDAO;
import com.utepsa.db.document.DocumentDAO;
import com.utepsa.db.document.DocumentRealDAO;
import com.utepsa.db.documentStudent.DocumentStudentDAO;
import com.utepsa.db.documentStudent.DocumentStudentRealDAO;
import com.utepsa.db.historyNote.HistoryNotesDAO;
import com.utepsa.db.historyNote.HistoryNotesRealDAO;
import com.utepsa.db.notification.NotificationDAO;
import com.utepsa.db.notification.NotificationRealDAO;
import com.utepsa.db.permission.PermissionDAO;
import com.utepsa.db.permission.PermissionRealDAO;
import com.utepsa.db.permissionCredential.PermissionCredentialDAO;
import com.utepsa.db.permissionCredential.PermissionCredentialRealDAO;
import com.utepsa.db.permissionRole.PermissionRoleDAO;
import com.utepsa.db.permissionRole.PermissionRoleRealDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.db.student.StudentRealDAO;
import org.hibernate.SessionFactory;

/**
 * Created by David-SW on 08/05/2017.
 */
public class HbnModule extends AbstractModule {
    private final HbnBundle hbnBundle;

    public HbnModule(HbnBundle hbnBundle) {
        this.hbnBundle = hbnBundle;
    }

    @Override
    protected void configure() {
        bind(SessionFactory.class).toInstance(hbnBundle.getSessionFactory());
        bind(CourseDAO.class).toInstance(new CourseRealDAO(hbnBundle.getSessionFactory()));
        bind(CareerCourseDAO.class).toInstance(new CareerCourseRealDAO(hbnBundle.getSessionFactory()));
        bind(CareerDAO.class).toInstance(new CareerRealDAO(hbnBundle.getSessionFactory()));
        bind(CategoryNotificationDAO.class).toInstance(new CategoryNotificationRealDAO(hbnBundle.getSessionFactory()));
        bind(CredentialDAO.class).toInstance(new CredentialRealDAO(hbnBundle.getSessionFactory()));
        bind(CurrentSemesterDAO.class).toInstance(new CurrentSemesterRealDAO(hbnBundle.getSessionFactory()));
        bind(DocumentDAO.class).toInstance(new DocumentRealDAO(hbnBundle.getSessionFactory()));
        bind(DocumentStudentDAO.class).toInstance(new DocumentStudentRealDAO(hbnBundle.getSessionFactory()));
        bind(HistoryNotesDAO.class).toInstance(new HistoryNotesRealDAO(hbnBundle.getSessionFactory()));
        bind(NotificationDAO.class).toInstance(new NotificationRealDAO(hbnBundle.getSessionFactory()));
        bind(PermissionDAO.class).toInstance(new PermissionRealDAO(hbnBundle.getSessionFactory()));
        bind(PermissionRoleDAO.class).toInstance(new PermissionRoleRealDAO(hbnBundle.getSessionFactory()));
        bind(PermissionCredentialDAO.class).toInstance(new PermissionCredentialRealDAO(hbnBundle.getSessionFactory()));
        bind(StudentDAO.class).toInstance(new StudentRealDAO(hbnBundle.getSessionFactory()));
    }
}
