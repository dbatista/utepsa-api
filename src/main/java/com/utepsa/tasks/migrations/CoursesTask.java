package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.courses.CoursesAdapter;
import com.utepsa.adapters.utepsa.courses.CoursesDATA;
import com.utepsa.config.ExternalServer;
import com.utepsa.db.career.CareerDAO;
import com.utepsa.db.careerCourse.CareerCourseDAO;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.models.Career;
import com.utepsa.models.CareerCourse;
import com.utepsa.models.Course;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by David on 28/01/2017.
 */
public class CoursesTask extends Task {
    @Inject
    private SessionFactory sessionFactory;
    @Inject
    private CourseDAO courseDAO;
    @Inject
    private CoursesAdapter coursesAdapter;

    public CoursesTask() {
        super("course_migration");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                registerCourses();
                transaction.commit();
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void registerCourses() throws Exception {
        List<CoursesDATA> coursesUtepsa = coursesAdapter.getAllCourses();
        for(CoursesDATA courseUtepsa : coursesUtepsa) {
            if (courseDAO.getByCodeUtepsa(courseUtepsa.getMat_codigo().trim()) == null){
                Course course = new Course();
                course.setName(courseUtepsa.getMat_descripcion().trim());
                course.setCodeUtepsa(courseUtepsa.getMat_codigo());
                course.setInitials(courseUtepsa.getMat_sigla().trim());
                courseDAO.create(course);
            }
        }
    }


}
