package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentAdapter;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentData;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesAdapter;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesData;
import com.utepsa.adapters.utepsa.students.StudentAdapter;
import com.utepsa.adapters.utepsa.students.StudentData;
import com.utepsa.config.ExternalServer;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.document.DocumentDAO;
import com.utepsa.db.documentStudent.DocumentStudentDAO;
import com.utepsa.db.historyNote.HistoryNotesDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.*;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Luana Chavez on 27/03/2017.
 */
public class SynchronizeInformationByStudentTask extends Task{
    @Inject
    private SessionFactory sessionFactory;
    @Inject
    private StudentDAO studentDAO;
    @Inject
    private StudentAdapter studentAdapter;

    @Inject
    private CourseDAO courseDAO;
    @Inject
    private HistoryNotesDAO historyNotesDAO;
    @Inject
    private HistoryNotesAdapter historyNotesAdapter;

    @Inject
    private DocumentDAO documentDAO;
    @Inject
    private DocumentStudentDAO documentStudentDAO;
    @Inject
    private DocumentsStudentAdapter documentsStudentAdapter;


    public SynchronizeInformationByStudentTask() {
        super("sync_student_information");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try
        {
            ManagedSessionContext.bind(session);
            String register = immutableMultimap.get("register").toArray()[0].toString();
            Student student = studentDAO.getByRegisterNumber(register.trim());
            if(student==null){
                printWriter.write("No existe estudiante con este registro \n");
                return;
            }
            else{
                transaction = session.beginTransaction();
                boolean synchronizedStudent = false;
                this.SyncStudent(student);
                do{
                    try{
                        this.SyncHistoryNotes(student);
                        synchronizedStudent = true;
                    }catch (Exception e){
                        Thread.sleep(5000);
                        printWriter.write(e.getMessage() + " \n");
                    }
                }while (!synchronizedStudent);

                synchronizedStudent = false;
                do{
                    try{
                        this.SyncDocumentsStudent(student);
                        synchronizedStudent = true;
                    }catch (Exception e){
                        Thread.sleep(5000);
                        printWriter.write(e.getMessage() + " \n");
                    }
                }while (!synchronizedStudent);

                transaction.commit();
            }


        }catch (Exception e){
            if ( transaction.getStatus() == TransactionStatus.ACTIVE ) {
                transaction.rollback();
            }
        }finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }

    public void SyncStudent(Student student) throws Exception{
        StudentData data = studentAdapter.getStudent(student.getRegisterNumber());

        student.setName(data.getAgd_nombres().trim());
        student.setFatherLastname(data.getAgd_appaterno().trim());
        student.setMotherLastname(data.getAgd_apmaterno().trim());
        student.setPhoneNumber1(data.getAgd_telf1().trim());
        student.setPhoneNumber2(data.getAgd_telf2().trim());
        student.setEmail1(data.getCorreo().trim());
    }

    private void createHistoryNote(HistoryNotesData data,long idStudent) throws Exception {
        Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());
        HistoryNotes historyNote = new HistoryNotes();
        historyNote.setIdStudent(idStudent);
        historyNote.setCourse(course);
        historyNote.setMinimunNote((int) data.getPln_notaminima());
        historyNote.setNote((int) data.getNot_nota());
        historyNote.setModule(data.getMdu_codigo().trim());
        historyNote.setSemester(data.getSem_codigo().trim());
        historyNotesDAO.create(historyNote);
    }

    private void updateHistoryNote(HistoryNotesData data, List<HistoryNotes> historyNotesApp , long idStudent) throws Exception {
        Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());
        for(HistoryNotes historyNote: historyNotesApp){
            if(historyNote.getSemester().equals(data.getSem_codigo()) && historyNote.getModule().equals(data.getMdu_codigo()) && historyNote.getCourse().getId() == course.getId()){
                if(historyNote.getNote() != data.getNot_nota()){
                    historyNote.setNote((int) data.getNot_nota());
                }
                return;
            }
        }

        this.createHistoryNote(data, idStudent);
    }

    private void createDocumentStudent(DocumentsStudentData documentsStudentData, long idStudent) throws Exception {
        DocumentStudent documentStudent = new DocumentStudent();

        Document document = documentDAO.getByCodeUtepsa(documentsStudentData.getDoc_codigo().trim());
        documentStudent.setIdStudent(idStudent);
        documentStudent.setTypePaper(documentsStudentData.getAlmdoc_tipopapel().trim());
        documentStudent.setDocument(document);

        if(documentsStudentData.getESTADO().trim().contains("ENTREGADO")){
            documentStudent.setState(true);
        }else {
            documentStudent.setState(false);
        }
        if(documentsStudentData.getAlmdoc_fechaentregar() == null){
            documentStudent.setDateDelivery(null);
        }
        else{
            SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
            Date dateDelivery = formaterTimeZone.parse(documentsStudentData.getAlmdoc_fechaentregar().trim());
            documentStudent.setDateDelivery(dateDelivery);
        }

        if(documentsStudentData.getAlmdoc_fecharecepcion() == null){
            documentStudent.setDateReceipt(null);
        }
        else{
            SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
            Date dateReceipt = formaterTimeZone.parse(documentsStudentData.getAlmdoc_fecharecepcion().trim());
            documentStudent.setDateDelivery(dateReceipt);
        }

        documentStudentDAO.create(documentStudent);
    }

    private void updateDocumentStudent(DocumentsStudentData data, List<DocumentStudent> documentsStudentApp , long idStudent) throws Exception {
        Document document = documentDAO.getByCodeUtepsa(data.getDoc_codigo().trim());

        for(DocumentStudent documentStudent: documentsStudentApp){
            if(documentStudent.getDocument().getId() == document.getId()){
                if(!documentStudent.getTypePaper().equals(data.getAlmdoc_tipopapel().trim())){
                    documentStudent.setTypePaper(data.getAlmdoc_tipopapel().trim());
                }


                SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
                if(data.getAlmdoc_fechaentregar() != null){
                    Date dateDelivery = formaterTimeZone.parse(data.getAlmdoc_fechaentregar().trim());

                    if(documentStudent.getDateDelivery() != dateDelivery){
                        documentStudent.setDateDelivery(dateDelivery);
                    }
                }

                if(data.getAlmdoc_fecharecepcion() != null){
                    Date dateReceipt = formaterTimeZone.parse(data.getAlmdoc_fecharecepcion().trim());
                    if(documentStudent.getDateReceipt() != dateReceipt){
                        documentStudent.setDateReceipt(dateReceipt);
                    }
                }

                boolean state;
                if(data.getESTADO().trim().contains("ENTREGADO")){
                    state = true;
                }else{
                    state = false;
                }

                if(documentStudent.isState() != state){
                    documentStudent.setState(state);
                }
                return;
            }
        }

        this.createDocumentStudent(data, idStudent);
    }

    public void SyncDocumentsStudent(Student student) throws Exception{
        List<DocumentStudent> documentsStudentApp = documentStudentDAO.getByStudent(student.getId());
        List<DocumentsStudentData> documentsStudentUtepsa = documentsStudentAdapter.getAllDocumentsStudentForStudent(student.getRegisterNumber());

        if(documentsStudentApp.size() == 0 && documentsStudentUtepsa.size() > 0){
            for(DocumentsStudentData data: documentsStudentUtepsa){
                createDocumentStudent(data, student.getId());
            }
        }

        if(documentsStudentUtepsa.size() <= 0) return;

        for(DocumentsStudentData data: documentsStudentUtepsa){
            updateDocumentStudent(data, documentsStudentApp, student.getId());
        }

    }

    public void SyncHistoryNotes(Student student) throws Exception {
        List<HistoryNotesData> historyNotesUtepsa = historyNotesAdapter.getAllHistoryNotesStudent(student.getRegisterNumber());
        List<HistoryNotes> historyNotesApp = historyNotesDAO.getByStudent(student.getId());

        if(historyNotesApp.size() == 0 && historyNotesUtepsa.size() > 0){
            for(HistoryNotesData data : historyNotesUtepsa){
                createHistoryNote(data, student.getId());
            }
            return;
        }

        if(historyNotesUtepsa.size() <= 0) return;

        for(HistoryNotesData data : historyNotesUtepsa){
            this.updateHistoryNote(data, historyNotesApp, student.getId());
        }
    }
}
