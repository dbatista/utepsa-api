package com.utepsa.tasks.migrations;

import com.google.common.collect.ImmutableMultimap;
import com.google.inject.Inject;
import com.utepsa.core.Operations;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.models.Credential;
import io.dropwizard.servlets.tasks.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.io.PrintWriter;
import java.util.List;
import java.util.Optional;

/**
 * Created by David on 08/10/2017.
 */
public class EncryptPasswordTask extends Task {

    @Inject
    private SessionFactory sessionFactory;
    @Inject
    private CredentialDAO credentialDAO;

    protected EncryptPasswordTask() {
        super("encrypt_password");
    }

    @Override
    public void execute(ImmutableMultimap<String, String> immutableMultimap, PrintWriter printWriter) throws Exception {
        Session session = sessionFactory.openSession();

        try {
            ManagedSessionContext.bind(session);
            Transaction transaction = session.beginTransaction();
            try {
                List<Credential> credentials = credentialDAO.getAll();

                if(credentials != null){
                    credentials.forEach(credential -> {
                        if(!Optional.ofNullable(credential.getPassword()).isPresent() || credential.getPassword().isEmpty()){
                            credential.setPassword(Operations.encryptPassword("utepsa"));
                        }else{
                            credential.setPassword(Operations.encryptPassword(credential.getPassword().trim()));
                        }
                    });
                    transaction.commit();
                }
            }
            catch (Exception e) {
                transaction.rollback();
                throw new RuntimeException(e);
            }
        } finally {
            session.close();
            ManagedSessionContext.unbind(sessionFactory);
        }
    }
}
