package com.utepsa;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.toastshaman.dropwizard.auth.jwt.JwtAuthFilter;
import com.utepsa.auth.ApiAuthenticator;
import com.utepsa.auth.ApiAuthorizer;
import com.utepsa.auth.ApiKey;
import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.db.credential.CredentialRealDAO;
import com.utepsa.models.Credential;
import com.utepsa.modules.HbnBundle;
import com.utepsa.modules.HbnModule;
import com.utepsa.config.NotificationServerConfig;
import com.utepsa.modules.MainModule;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.UnitOfWorkAwareProxyFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.HmacKey;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class NotificationServerApp extends Application<NotificationServerConfig> {

    private HbnBundle hibernate;

    public static void main(String[] args) throws Exception {
        new NotificationServerApp().run(args);
    }

    @Override
    public String getName() {
        return "Notification Server";
    }

    @Override
    public void initialize(final Bootstrap<NotificationServerConfig> bootstrap) {
        hibernate = new HbnBundle();
        bootstrap.addBundle(new AssetsBundle("/assets/", "/"));
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(GuiceBundle.builder()
                .enableAutoConfig(getClass().getPackage().getName())
                .modules(new HbnModule(hibernate), new MainModule())
                .build());
        bootstrap.addBundle(new MigrationsBundle<NotificationServerConfig>() {
            @Override
            public DataSourceFactory getDataSourceFactory(NotificationServerConfig configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(final NotificationServerConfig configuration,
                    final Environment environment) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        this.setCORSconfiguration(environment);
        // ######################################
        // ########### START SECURITY #############
        // ######################################
        CredentialDAO credentialDAO = new CredentialRealDAO(hibernate.getSessionFactory());
        ApiAuthenticator authenticator = new UnitOfWorkAwareProxyFactory(hibernate)
            .create(ApiAuthenticator.class, CredentialDAO.class, credentialDAO);

        final JwtConsumer consumer = new JwtConsumerBuilder()
            .setAllowedClockSkewInSeconds(30)
            .setRequireExpirationTime()
            .setRequireSubject()
            .setVerificationKey(new HmacKey(ApiKey.getKeySHA256()))
            .setRelaxVerificationKeyValidation()
            .build();

        environment.jersey().register(new AuthDynamicFeature(
            new JwtAuthFilter.Builder<Credential>()
                .setJwtConsumer(consumer)
                .setRealm("realm")
                .setPrefix("Bearer")
                .setAuthenticator(authenticator)
                .setAuthorizer(new ApiAuthorizer())
                .buildAuthFilter()));

        environment.jersey().register(RolesAllowedDynamicFeature.class);
        environment.jersey().register(new AuthValueFactoryProvider.Binder<>(Credential.class));

        environment.jersey().register(MultiPartFeature.class);
        // ######################################
        // ########### END SECURITY #############
        // ######################################
        //initializing apiListingResource
        environment.jersey().register(new ApiListingResource());
        environment.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
        BeanConfig config = new BeanConfig();
        config.setTitle("UTEPSA API");
        config.setVersion("1.0.0");
        config.setBasePath("/api");
        config.setResourcePackage(getClass().getPackage().getName());
        config.setScan(true);
    }

    private void setCORSconfiguration(Environment environment) {
        FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter(CrossOriginFilter.EXPOSED_HEADERS_PARAM,
                "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Location,username,password");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM,
                "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Location,username,password");
        filter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
    }
}
