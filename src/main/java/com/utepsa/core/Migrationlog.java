package com.utepsa.core;

/**
 * Created by David on 29/01/2017.
 */
public class Migrationlog {
    public final static int MIGRATION_STUDENT_WITH_CREDENTIAL = 1;
    public final static int MIGRATION_STUDENT_HISTORY_NOTE = 2;
    public final static int MIGRATION_STUDENT_DOCUMENTS = 3;
    public final static int UPDATE_STUDENT_HISTORY_NOTE = 4;
    public final static int UPDATE_STUDENT_DOCUMENTS = 5;
    public final static int MIGRATION_STUDENT_COURSE_REGISTERED = 6;
    public final static int UPDATE_STUDENT_COURSE_REGISTERED = 7;
}
