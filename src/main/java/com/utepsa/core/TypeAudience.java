package com.utepsa.core;

/**
 * Created by David on 24/01/2017.
 */
public class TypeAudience {
    final public static long PUBLIC = 1;
    final public static long STUDENT = 2;
    final public static long CAREER = 3;
    final public static long GROUP = 4;
}
