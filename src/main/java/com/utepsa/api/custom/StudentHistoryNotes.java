package com.utepsa.api.custom;

import com.utepsa.models.Course;

import java.util.List;

/**
 * Created by David on 23/01/2017.
 */
public class StudentHistoryNotes<T> {
    private int approved;
    private int disapproved;
    private double average;
    private int total;
    private double progressPercentage;
    private List<T> courses;
    private List<Course> coursesMissing;

    public StudentHistoryNotes() {
    }

    public int getApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public int getDisapproved() {
        return disapproved;
    }

    public void setDisapproved(int disapproved) {
        this.disapproved = disapproved;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public double getProgressPercentage() {
        return progressPercentage;
    }

    public void setProgressPercentage(double progressPercentage) {
        this.progressPercentage = progressPercentage;
    }

    public List<T> getCourses() {
        return courses;
    }

    public void setCourses(List<T> courses) {
        this.courses = courses;
    }

    public List<Course> getCoursesMissing() {
        return coursesMissing;
    }

    public void setCoursesMissing(List<Course> coursesMissing) {
        this.coursesMissing = coursesMissing;
    }
}
