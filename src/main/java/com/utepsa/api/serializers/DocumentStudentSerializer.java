package com.utepsa.api.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.utepsa.models.DocumentStudent;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Created by David on 26/01/2017.
 */
public class DocumentStudentSerializer extends JsonSerializer<DocumentStudent> {
    @Override
    public void serialize(DocumentStudent documentStudent, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("idDocument", documentStudent.getDocument().getId());
        jsonGenerator.writeStringField("document", documentStudent.getDocument().getDescription());

        SimpleDateFormat formaterShortDate = new SimpleDateFormat("dd/MM/yyyy");
        if(documentStudent.getDateDelivery() != null) {
            String dateDelivery = formaterShortDate.format(documentStudent.getDateDelivery());
            jsonGenerator.writeStringField("dateDelivery", dateDelivery);
        }else{
            jsonGenerator.writeStringField("dateDelivery", null);
        }

        if(documentStudent.getDateReceipt() != null) {
            String dateReceipt = formaterShortDate.format(documentStudent.getDateReceipt());
            jsonGenerator.writeStringField("dateReceipt", dateReceipt);
        }else{
            jsonGenerator.writeStringField("dateReceipt", null);
        }
        jsonGenerator.writeStringField("typePaper", documentStudent.getTypePaper());
        jsonGenerator.writeBooleanField("state", documentStudent.isState());
        jsonGenerator.writeNumberField("related", documentStudent.getDocument().getRelated());
        jsonGenerator.writeEndObject();
    }
}
