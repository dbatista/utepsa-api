package com.utepsa.api.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utepsa.api.response.BasicResponse;

import java.io.IOException;
import java.util.List;

/**
 * Created by David on 07/05/2017.
 */
public class BasicResponseDeserializer<T> extends JsonDeserializer<BasicResponse> {
    @Override
    public BasicResponse deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        ObjectCodec oc = jsonParser.getCodec();

        JsonNode node = oc.readTree(jsonParser);

        final long code = node.get("code").asLong();
        final String status = node.get("status").asText();
        String message = "";
        if(node.get("message") != null)
            message = node.get("message").asText();

        return new BasicResponse(code, status, message, null, null, null);
    }
}
