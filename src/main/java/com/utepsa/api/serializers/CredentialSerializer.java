package com.utepsa.api.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.utepsa.models.Credential;

import java.io.IOException;

/**
 * Created by David on 27/01/2017.
 */
public class CredentialSerializer extends JsonSerializer<Credential> {
    @Override
    public void serialize(Credential credential, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id",credential.getId());
        jsonGenerator.writeNumberField("idRole",credential.getRole().getId());
        jsonGenerator.writeStringField("role",credential.getRole().getName());
        jsonGenerator.writeObjectField("student",credential.getStudent());
        jsonGenerator.writeEndObject();
    }
}
