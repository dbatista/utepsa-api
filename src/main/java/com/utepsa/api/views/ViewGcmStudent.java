package com.utepsa.api.views;

import org.hibernate.annotations.Subselect;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "com.utepsa.api.views.ViewGcmStudent.getAll",
            query = "SELECT v.gcmId FROM ViewGcmStudent v " +
                    "WHERE v.gcmId <> '' AND v.gcmId <> 'NOT REGISTERED' AND v.gcmId <> null"
    ),
    @NamedQuery(
            name = "com.utepsa.api.views.ViewGcmStudent.getByStudent",
            query = "SELECT v.gcmId FROM ViewGcmStudent v " +
                    "WHERE v.gcmId <> '' AND v.gcmId <> 'NOT REGISTERED' AND v.gcmId <> null AND v.student = :idStudent"
    ),
    @NamedQuery(
            name = "com.utepsa.api.views.ViewGcmStudent.getByCareer",
            query = "SELECT v.gcmId FROM ViewGcmStudent v " +
                    "WHERE v.gcmId <> '' AND v.gcmId <> 'NOT REGISTERED' AND v.gcmId <> null AND v.idCareer = :idCareer"
    )
})
@Subselect("select * from view_gcm_student")
public class ViewGcmStudent {
    private long user;
    private long student;
    private long idCareer;
    private String gcmId;

    @Id
    @GeneratedValue
    @Column(name = "id_credential", updatable = false, nullable = false)
    public long getUser() {
        return user;
    }

    @Column(name = "id_student", nullable = true, length = 20)
    public long getStudent() {
        return student;
    }

    @Column(name = "id_career", nullable = true)
    public long getIdCareer() {
        return idCareer;
    }

    @Column(name = "gcm_id", nullable = true, length = 255)
    public String getGcmId() {
        return gcmId;
    }

    public void setUser(long user) {
        this.user = user;
    }

    public void setStudent(long student) {
        this.student = student;
    }

    public void setIdCareer(long idCareer) {
        this.idCareer = idCareer;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

}
