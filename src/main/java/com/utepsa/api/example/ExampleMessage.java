package com.utepsa.api.example;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by roberto on 7/7/2016.
 */
public class ExampleMessage {

    private int id;
    private String message;
    private String serverName;

    public ExampleMessage() {}

    public ExampleMessage(int id, String message, String serverName) {
        this.id = id;
        this.message = message;
        this.serverName = serverName;
    }

    @JsonProperty
    public int getId() {
        return this.id;
    }

    @JsonProperty
    public String getMessage() {
        return this.message;
    }

    @JsonProperty
    public String getServerName() {
        return this.serverName;
    }
}
