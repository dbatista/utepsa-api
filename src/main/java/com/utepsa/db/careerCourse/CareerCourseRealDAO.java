package com.utepsa.db.careerCourse;

import com.google.inject.Inject;
import com.utepsa.models.CareerCourse;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by David on 22/01/2017.
 */
public class CareerCourseRealDAO extends AbstractDAO<CareerCourse> implements CareerCourseDAO {
    @Inject
    public CareerCourseRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public void create(CareerCourse careerCourse) throws Exception{
        final CareerCourse created = persist(careerCourse);
        if(created == null) throw new Exception("The career was not created.");
    }

    @Override
    public List<CareerCourse> getAll() {
        return currentSession().createCriteria(CareerCourse.class).list();
    }

    @Override
    public List<CareerCourse> getCoursesByCareer(long idCareer, int pensum) throws Exception {
        return list(namedQuery("com.utepsa.models.CareerCourse.getCoursesByCareer")
                .setParameter("idCareer", idCareer)
                .setParameter("pensum", pensum));
    }

    @Override
    public CareerCourse getCourseByCoursePensum(long idCourse, long idCareer, int pensum) throws Exception {
        return uniqueResult(namedQuery("com.utepsa.models.CareerCourse.getCourseByCoursePensum")
                .setParameter("idCourse", idCourse)
                .setParameter("idCareer", idCareer)
                .setParameter("pensum", pensum));
    }
}
