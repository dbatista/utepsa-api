package com.utepsa.db.credential;

import com.utepsa.models.Credential;

import java.util.List;
import java.util.Optional;

/**
 * Created by david on 19/10/16.
 */
public interface CredentialDAO {
    long create(Credential credential);
    Credential login(String username, String password) throws Exception;
    Credential getById(long id) throws Exception;
    void registerLastConnection(Credential credential) throws Exception;
    void registerGcmId(long idCredential, String gcmId) throws Exception;
    List<String> getAllGcmId() throws Exception;
    List<String> getGcmIdByCareer(long idCareer) throws Exception;
    List<String> getGcmIdByStudent(long idStudent) throws Exception;
    void changePasswordForced(Credential credential, String newPassword) throws Exception;
    Optional<Credential> getByUsername(String username);
    List<Credential> getAll();
}
