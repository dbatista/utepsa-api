package com.utepsa.db.historyNote;


import com.utepsa.models.Course;
import com.utepsa.models.HistoryNotes;

import java.util.List;

/**
 * Created by Alexis Ardaya on 10/12/2016.
 */
public interface HistoryNotesDAO {

    long create(HistoryNotes historyNotes) throws Exception;
    List<HistoryNotes> getByStudent(long idStudent) throws Exception;
    List<Course> getCoursesMissingByStudent(long idStudent, long idCareer, long pensum) throws Exception;
    List getHistoyryNotesByPensum(long idStudent, long idCarrer, long idPensum) throws Exception;

}
