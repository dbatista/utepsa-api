package com.utepsa.db.course;

import com.utepsa.models.Course;

/**
 * Created by David on 18/01/2017.
 */
public interface CourseDAO {
    long create(Course course) throws Exception;
    Course getById(long id) throws Exception;
    Course getByCodeUtepsa(String codeUtepsa) throws Exception;
}
