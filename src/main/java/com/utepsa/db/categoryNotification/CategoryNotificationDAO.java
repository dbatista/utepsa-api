package com.utepsa.db.categoryNotification;

import com.utepsa.models.CategoryNotification;

/**
 * Created by Luana Chavez on 11/04/2017.
 */
public interface CategoryNotificationDAO {
    CategoryNotification getById(long id)throws Exception;
}
