package com.utepsa.adapters.gcm;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by roberto on 25/8/2016.
 */
public class Data {
    private String title;
    private String body;

    public Data() { }

    public Data(String title, String body) {
        this.title = title;
        this.body = body;
    }

    @JsonProperty
    public String getTitle() {
        return title;
    }

    @JsonProperty
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty
    public String getBody() {
        return body;
    }

    @JsonProperty
    public void setBody(String body) {
        this.body = body;
    }
}
