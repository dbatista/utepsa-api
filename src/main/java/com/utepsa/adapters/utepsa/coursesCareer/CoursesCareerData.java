package com.utepsa.adapters.utepsa.coursesCareer;

/**
 * Created by David-SW on 10/03/2017.
 */
public class CoursesCareerData {
    private String crr_codigo;
    private String pns_codigo;
    private String mat_codigo;
    private boolean pns_electiva;
    private int pns_nivel;
    private int pns_orden;

    public String getCrr_codigo() {
        return crr_codigo;
    }

    public void setCrr_codigo(String crr_codigo) {
        this.crr_codigo = crr_codigo;
    }

    public String getPns_codigo() {
        return pns_codigo;
    }

    public void setPns_codigo(String pns_codigo) {
        this.pns_codigo = pns_codigo;
    }

    public String getMat_codigo() {
        return mat_codigo;
    }

    public void setMat_codigo(String mat_codigo) {
        this.mat_codigo = mat_codigo;
    }

    public boolean isPns_electiva() {
        return pns_electiva;
    }

    public void setPns_electiva(boolean pns_electiva) {
        this.pns_electiva = pns_electiva;
    }

    public int getPns_nivel() {
        return pns_nivel;
    }

    public void setPns_nivel(int pns_nivel) {
        this.pns_nivel = pns_nivel;
    }

    public int getPns_orden() {
        return pns_orden;
    }

    public void setPns_orden(int pns_orden) {
        this.pns_orden = pns_orden;
    }
}
