package com.utepsa.adapters.utepsa.courses;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by shigeots on 21-11-16.
 */
public class CoursesDATA {
    private String mat_codigo;
    private String mat_sigla;
    private String mat_descripcion;
    private long aac_codigo;

    public CoursesDATA() {
    }

    public CoursesDATA(String mat_codigo, String mat_sigla, String mat_descripcion) {
        this.mat_codigo = mat_codigo;
        this.mat_sigla = mat_sigla;
        this.mat_descripcion = mat_descripcion;
    }

    public String getMat_codigo() {
        return mat_codigo;
    }

    public void setMat_codigo(String mat_codigo) {
        this.mat_codigo = mat_codigo;
    }

    public String getMat_sigla() {
        return mat_sigla;
    }

    public void setMat_sigla(String mat_sigla) {
        this.mat_sigla = mat_sigla;
    }

    public String getMat_descripcion() {
        return mat_descripcion;
    }

    public void setMat_descripcion(String mat_descripcion) {
        this.mat_descripcion = mat_descripcion;
    }

    public long getAac_codigo() {
        return aac_codigo;
    }

    public void setAac_codigo(long aac_codigo) {
        this.aac_codigo = aac_codigo;
    }
}
