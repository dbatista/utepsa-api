package com.utepsa.adapters.utepsa.careers;

/**
 * Created by David on 09/11/2016.
 */
public class CareersDATA {
    private String crr_codigo;
    private String crr_descripcion;
    private String crr_pensumactual;
    private String sca_descripcion;

    public CareersDATA() {
    }

    public CareersDATA(String crr_codigo, String crr_descripcion, String crr_pensumactual, String sca_descripcion) {
        this.crr_codigo = crr_codigo;
        this.crr_descripcion = crr_descripcion;
        this.crr_pensumactual = crr_pensumactual;
        this.sca_descripcion = sca_descripcion;
    }

    public String getCrr_codigo() {
        return crr_codigo;
    }

    public void setCrr_codigo(String crr_codigo) {
        this.crr_codigo = crr_codigo;
    }

    public String getCrr_descripcion() {
        return crr_descripcion;
    }

    public void setCrr_descripcion(String crr_descripcion) {
        this.crr_descripcion = crr_descripcion;
    }

    public String getCrr_pensumactual() {
        return crr_pensumactual;
    }

    public void setCrr_pensumactual(String crr_pensumactual) {
        this.crr_pensumactual = crr_pensumactual;
    }

    public String getSca_descripcion() {
        return sca_descripcion;
    }

    public void setSca_descripcion(String sca_descripcion) {
        this.sca_descripcion = sca_descripcion;
    }

    @Override
    public String toString() {
        return "CareersDATA{" +
                "crr_codigo='" + crr_codigo + '\'' +
                ", crr_descripcion='" + crr_descripcion + '\'' +
                ", crr_pensumactual='" + crr_pensumactual + '\'' +
                ", sca_descripcion='" + sca_descripcion + '\'' +
                '}';
    }
}
