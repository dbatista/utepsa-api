package com.utepsa.adapters.utepsa.studentCourseRegister;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Luana Chavez on 02/03/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentCourseRegisterData {

    private String alm_registro;
    private String sem_codigo;
    private String mdu_codigo;
    private String mat_codigo;
    private String mat_descripcion;
    private String aul_codigo;
    private long grp_nroinscritos;
    private String mat_sigla;
    private String observacion;
    private String agd_docente;
    private String grp_grupo;
    private String tur_codigo;
    private long not_nota;
    private long grp_detalle_id;

    public StudentCourseRegisterData() {
    }

    public StudentCourseRegisterData(String alm_registro, String sem_codigo, String mdu_codigo, String mat_codigo, String mat_descripcion, String aul_codigo, long grp_nroinscritos, String mat_sigla, String observacion, String agd_docente, String grp_grupo, String tur_codigo, long not_nota, long grp_detalle_id) {
        this.alm_registro = alm_registro;
        this.sem_codigo = sem_codigo;
        this.mdu_codigo = mdu_codigo;
        this.mat_codigo = mat_codigo;
        this.mat_descripcion = mat_descripcion;
        this.aul_codigo = aul_codigo;
        this.grp_nroinscritos = grp_nroinscritos;
        this.mat_sigla = mat_sigla;
        this.observacion = observacion;
        this.agd_docente = agd_docente;
        this.grp_grupo = grp_grupo;
        this.tur_codigo = tur_codigo;
        this.not_nota = not_nota;
        this.grp_detalle_id = grp_detalle_id;
    }

    public String getAlm_registro() {
        return alm_registro;
    }

    public void setAlm_registro(String alm_registro) {
        this.alm_registro = alm_registro;
    }

    public String getSem_codigo() {
        return sem_codigo;
    }

    public void setSem_codigo(String sem_codigo) {
        this.sem_codigo = sem_codigo;
    }

    public String getMdu_codigo() {
        return mdu_codigo;
    }

    public void setMdu_codigo(String mdu_codigo) {
        this.mdu_codigo = mdu_codigo;
    }

    public String getMat_codigo() {
        return mat_codigo;
    }

    public void setMat_codigo(String mat_codigo) {
        this.mat_codigo = mat_codigo;
    }

    public String getMat_descripcion() {
        return mat_descripcion;
    }

    public void setMat_descripcion(String mat_descripcion) {
        this.mat_descripcion = mat_descripcion;
    }

    public String getAul_codigo() {
        return aul_codigo;
    }

    public void setAul_codigo(String aul_codigo) {
        this.aul_codigo = aul_codigo;
    }

    public long getGrp_nroinscritos() {
        return grp_nroinscritos;
    }

    public void setGrp_nroinscritos(long grp_nroinscritos) {
        this.grp_nroinscritos = grp_nroinscritos;
    }

    public String getMat_sigla() {
        return mat_sigla;
    }

    public void setMat_sigla(String mat_sigla) {
        this.mat_sigla = mat_sigla;
    }

    public String getAgd_docente() {
        return agd_docente;
    }

    public void setAgd_docente(String agd_docente) {
        this.agd_docente = agd_docente;
    }

    public String getTur_codigo() {
        return tur_codigo;
    }

    public void setTur_codigo(String tur_codigo) {
        this.tur_codigo = tur_codigo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getGrp_grupo() {
        return grp_grupo;
    }

    public void setGrp_grupo(String grp_grupo) {
        this.grp_grupo = grp_grupo;
    }

    public long getNot_nota() {
        return not_nota;
    }

    public void setNot_nota(long not_nota) {
        this.not_nota = not_nota;
    }

    public long getGrp_detalle_id() {
        return grp_detalle_id;
    }

    public void setGrp_detalle_id(long grp_detalle_id) {
        this.grp_detalle_id = grp_detalle_id;
    }
}
