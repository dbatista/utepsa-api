package com.utepsa.models;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Embeddable
public class NotificationAudience {

    private String target;
    private TypeAudience type;

    public NotificationAudience() {
    }

    public NotificationAudience(String target, TypeAudience type) {
        this.target = target;
        this.type = type;
    }

    @Basic
    @Column(name = "target", nullable = false)
    @ApiModelProperty(value = "The target of the notification in application", required = true)
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @ManyToOne
    @JoinColumn(name = "id_type", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The audience of the notification in application", required = true)
    public TypeAudience getType() {
        return type;
    }

    public void setType(TypeAudience type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotificationAudience that = (NotificationAudience) o;

        if (target != null ? !target.equals(that.target) : that.target != null) return false;
        return type != null ? type.equals(that.type) : that.type == null;
    }

    @Override
    public int hashCode() {
        int result = target != null ? target.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NotificationAudience{" +
                "target='" + target + '\'' +
                ", type=" + type +
                '}';
    }
}
