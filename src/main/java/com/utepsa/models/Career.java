package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;


/**
 * Created by David on 22/01/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.Career.getByCodeUtepsa",
                query = "SELECT c FROM Career c WHERE codeUtepsa = :codeUtepsa"
        ),
        @NamedQuery(
                name = "com.utepsa.models.Career.getByGrade",
                query = "select c from Career c INNER JOIN c.faculty f " +
                        "WHERE f.grade = :grade"
        )
})
@ApiModel(value = "Career entity", description = "Complete info of a entity Career")
public class Career {

    private long id;
    private String codeUtepsa;
    private String name;
    private int currentPensum;
    private Faculty faculty;

    public Career() {
    }

    public Career(long id, String codeUtepsa, String name, int currentPensum, Faculty faculty) {
        this.id = id;
        this.codeUtepsa = codeUtepsa;
        this.name = name;
        this.currentPensum = currentPensum;
        this.faculty = faculty;
    }

    public Career(long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the Career in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonIgnore
    @Basic
    @Column(name = "code_utepsa", nullable = true, length = 10)
    @ApiModelProperty(value = "The code of the Career in Utepsa")
    public String getCodeUtepsa() {
        return codeUtepsa;
    }

    public void setCodeUtepsa(String codeUtepsa) {
        this.codeUtepsa = codeUtepsa;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    @ApiModelProperty(value = "The name of the Career in Utepsa", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "current_pensum", nullable = false)
    @ApiModelProperty(value = "The current pensum of the Career in Utepsa", required = true)
    public int getCurrentPensum() {
        return currentPensum;
    }

    public void setCurrentPensum(int currentPensum) {
        this.currentPensum = currentPensum;
    }

    @ManyToOne
    @JoinColumn(name = "id_faculty", referencedColumnName = "id", nullable = false)
    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Career career = (Career) o;

        if (id != career.id) return false;
        if (currentPensum != career.currentPensum) return false;
        if (name != null ? !name.equals(career.name) : career.name != null) return false;
        return faculty != null ? faculty.equals(career.faculty) : career.faculty == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + currentPensum;
        result = 31 * result + (faculty != null ? faculty.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Career{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", currentPensum=" + currentPensum +
                ", faculty=" + faculty +
                '}';
    }
}

