package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Table;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(name = "category_notification")
@ApiModel(value = "Category_notification entity", description = "Complete info of a entity Category_notification")
public class CategoryNotification {

    private long id;
    private String name;
    private CategoryNotification parent;

    public CategoryNotification() {
    }

    public CategoryNotification(long id, String name, CategoryNotification parent) {
        this.id = id;
        this.name = name;
        this.parent = parent;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the category_notification in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the category_notification in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToOne
    @JoinColumn(name = "id_parent", referencedColumnName = "id")
    @ApiModelProperty(value = "The parent of the category_notification in application", required = true)
    public CategoryNotification getParent() {
        return parent;
    }

    public void setParent(CategoryNotification parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryNotification that = (CategoryNotification) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return parent != null ? parent.equals(that.parent) : that.parent == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("category_notification{id=%d,name=%s,parent=%d}",
                this.id, this.name, this.parent);
    }
}
