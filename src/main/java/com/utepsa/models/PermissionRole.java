package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Gerardo on 14/02/2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.PermissionRole.getByIdRole",
                query = "SELECT pr FROM PermissionRole pr INNER JOIN pr.role r WHERE r.id = :idRole"
        ),
        @NamedQuery(
                name = "com.utepsa.models.PermissionRole.getByRoleAndPermission",
                query = "SELECT pr FROM PermissionRole pr INNER JOIN pr.role r INNER JOIN pr.permission p WHERE r.id = :idRole AND p.id = :idPermission"
        )
})
@Table(name = "permission_role")
@ApiModel(value = "role permission entity", description = "Complete info of a entity permission role")
public class PermissionRole {
    private long id;
    private Role role;
    private Permission permission;
    private boolean state;

    public PermissionRole(){

    }

    public PermissionRole (long id, Role role, Permission permission, boolean state){
        this.id = id;
        this.role = role;
        this.permission = permission;
        this.state = state;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the role permission in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "id_role", nullable = false)
    @ApiModelProperty(value = "The id role of the permission credential in application", required = true)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @ManyToOne
    @JoinColumn(name = "id_permission", nullable = false)
    @ApiModelProperty(value = "The id permission of the permission credential in application", required = true)
    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    @Basic
    @ApiModelProperty(value = "The state of the permission role in application", required = true)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionRole that = (PermissionRole) o;

        if (id != that.id) return false;
        if (state != that.state) return false;
        if (role != null ? !role.equals(that.role) : that.role != null) return false;
        return permission != null ? permission.equals(that.permission) : that.permission == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (permission != null ? permission.hashCode() : 0);
        result = 31 * result + (state ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("PermissionRole{id=%d,role=%s,permission=%s,state=%s}",this.id,this.role,this.permission,this.state);
    }
}
