package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.utepsa.api.serializers.CredentialSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.security.Principal;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(name = "credential")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.Credential.login",
                query = "SELECT u FROM Credential u WHERE u.username = :username AND u.password = :password"
        ),
        @NamedQuery(
                name = "com.utepsa.models.Credential.getByUsername",
                query = "SELECT u FROM Credential u WHERE u.username = :username"
        )
})
@JsonSerialize(using = CredentialSerializer.class)
@ApiModel(value = "Credential entity", description = "Complete info of a entity credential")
public class Credential implements Principal {

    private long id;
    private String username;
    private String password;
    private String token;
    private String gcmId;
    private String lastConnection;
    private boolean state;
    private Role role;
    private Student student;
    private boolean changePasswordForced;
    private String name;

    public Credential() {
    }

    public Credential(long id, String username, String password, String token, String gcmId, String lastConnection, boolean state, Role role, Student student, boolean changePasswordForced) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.token = token;
        this.gcmId = gcmId;
        this.lastConnection = lastConnection;
        this.state = state;
        this.role = role;
        this.student = student;
        this.changePasswordForced = changePasswordForced;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the credential in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "username", nullable = false)
    @ApiModelProperty(value = "The username of the credential in application", required = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = false)
    @ApiModelProperty(value = "The password of the credential in application", required = true)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "token", nullable = false)
    @ApiModelProperty(value = "The token of the credential in application", required = true)
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Basic
    @Column(name = "gcm_id", nullable = false)
    @ApiModelProperty(value = "The gcmId of the credential in application", required = true)
    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    @Basic
    @Column(name = "last_connection", nullable = false)
    @ApiModelProperty(value = "The lastConnection of the credential in application", required = true)
    public String getLastConnection() {
        return lastConnection;
    }

    public void setLastConnection(String lastConnection) {
        this.lastConnection = lastConnection;
    }

    @Basic
    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "The state of the credential in application", required = true)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @ManyToOne
    @JoinColumn(name = "id_role", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The role of the credential in application", required = true)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @ManyToOne
    @JoinColumn(name = "id_student", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The student of the credential in application", required = true)
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @JsonIgnore
    @Basic
    @Column(name = "change_password_forced", nullable = false)
    @ApiModelProperty(value = "The student of the credential in application", required = true)
    public boolean isChangePasswordForced() {
        return changePasswordForced;
    }

    public void setChangePasswordForced(boolean changePasswordForced) {
        this.changePasswordForced = changePasswordForced;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Credential that = (Credential) o;

        if (id != that.id) return false;
        if (state != that.state) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (token != null ? !token.equals(that.token) : that.token != null) return false;
        if (gcmId != null ? !gcmId.equals(that.gcmId) : that.gcmId != null) return false;
        if (lastConnection != null ? !lastConnection.equals(that.lastConnection) : that.lastConnection != null)
            return false;
        if (role != null ? !role.equals(that.role) : that.role != null) return false;
        return student != null ? student.equals(that.student) : that.student == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        result = 31 * result + (gcmId != null ? gcmId.hashCode() : 0);
        result = 31 * result + (lastConnection != null ? lastConnection.hashCode() : 0);
        result = 31 * result + (state ? 1 : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (student != null ? student.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Credential{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", token='" + token + '\'' +
                ", gcmId='" + gcmId + '\'' +
                ", lastConnection='" + lastConnection + '\'' +
                ", state=" + state +
                ", role=" + role +
                ", student=" + student +
                '}';
    }
    @JsonIgnore
    @Transient
    public String getName() {
        return this.username;
    }

    public void setName(String name){
        this.name = name;
    }
}
