package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@ApiModel(value = "faculty entity", description = "Complete info of a entity faculty")
public class Faculty {

    private long id;
    private String name;
    private String grade;
    private String abbreviation;

    public Faculty() {
    }

    public Faculty(long id) {
        this.id = id;
    }

    public Faculty(long id, String name, String grade, String abbreviation) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.abbreviation = abbreviation;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the faculty in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "The name of the faculty in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "abbreviation", nullable = false)
    @ApiModelProperty(value = "The abbreviation of the faculty in application", required = true)
    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Basic
    @Column(name = "grade", nullable = false)
    @ApiModelProperty(value = "The grade of the faculty in application", required = true)
    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Faculty faculty = (Faculty) o;

        if (id != faculty.id) return false;
        if (name != null ? !name.equals(faculty.name) : faculty.name != null) return false;
        if (grade != null ? !grade.equals(faculty.grade) : faculty.grade != null) return false;
        return abbreviation != null ? abbreviation.equals(faculty.abbreviation) : faculty.abbreviation == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (grade != null ? grade.hashCode() : 0);
        result = 31 * result + (abbreviation != null ? abbreviation.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", grade='" + grade + '\'' +
                ", abbreviation='" + abbreviation + '\'' +
                '}';
    }
}
