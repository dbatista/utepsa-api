package com.utepsa.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(name = "history_note")
@ApiModel(value = "HistoryNote entity", description = "Complete info of a entity HistoryNote")
public class HistoryNotes {

    private long id;
    private long idStudent;
    private Course course;
    private int note;
    private int minimunNote;
    private String semester;
    private String module;

    public HistoryNotes() {
    }

    public HistoryNotes(long id, long idStudent, Course course, int note, int minimunNote, String semester, String module) {
        this.id = id;
        this.idStudent = idStudent;
        this.course = course;
        this.note = note;
        this.minimunNote = minimunNote;
        this.semester = semester;
        this.module = module;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the history_note in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_student", nullable = false)
    @ApiModelProperty(value = "The idStudent of the history_note in application", required = true)
    public long getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(long idStudent) {
        this.idStudent = idStudent;
    }

    @ManyToOne
    @JoinColumn(name = "id_course", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The course of the history_note in application", required = true)
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Basic
    @Column(name = "note", nullable = false)
    @ApiModelProperty(value = "The note of the history_note in application", required = true)
    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    @Basic
    @Column(name = "minimun_note", nullable = false)
    @ApiModelProperty(value = "The minimunNote of the history_note in application", required = true)
    public int getMinimunNote() {
        return minimunNote;
    }

    public void setMinimunNote(int minimunNote) {
        this.minimunNote = minimunNote;
    }

    @Basic
    @Column(name = "semester", nullable = false, length = 50)
    @ApiModelProperty(value = "The semester of the history_note in application", required = true)
    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    @Basic
    @Column(name = "module", nullable = false, length = 20)
    @ApiModelProperty(value = "The module of the history_note in application", required = true)
    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoryNotes that = (HistoryNotes) o;

        if (id != that.id) return false;
        if (idStudent != that.idStudent) return false;
        if (note != that.note) return false;
        if (minimunNote != that.minimunNote) return false;
        if (course != null ? !course.equals(that.course) : that.course != null) return false;
        if (semester != null ? !semester.equals(that.semester) : that.semester != null) return false;
        return module != null ? module.equals(that.module) : that.module == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (idStudent ^ (idStudent >>> 32));
        result = 31 * result + (course != null ? course.hashCode() : 0);
        result = 31 * result + note;
        result = 31 * result + minimunNote;
        result = 31 * result + (semester != null ? semester.hashCode() : 0);
        result = 31 * result + (module != null ? module.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("historyNote{id=%d,idStudent=%d,course=%s,note=%d,minimunNote=%d," +
                "semester=%s,module=%s}",
                this.id,this.idStudent,this.course,this.note,this.minimunNote,this.semester,this.module);
    }
}
