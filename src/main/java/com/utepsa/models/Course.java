package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@Table(appliesTo = "Course")
@NamedQueries({
        @NamedQuery(
                name = "com.utepsa.models.Course.getByCodeUtepsa",
                query = "SELECT c FROM Course c WHERE codeUtepsa = :codeUtepsa"
        )
})
@ApiModel(value = "course entity", description = "Complete info of a entity course")
public class Course {

    private long id;
    private String codeUtepsa;
    private String name;
    private String initials;

    public Course() {
    }

    public Course(long id) {
        this.id = id;
    }

    public Course(long id, String codeUtepsa, String name, String initials) {
        this.id = id;
        this.codeUtepsa = codeUtepsa;
        this.name = name;
        this.initials = initials;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the course in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @JsonIgnore
    @Basic
    @Column(name = "code_utepsa", length = 10)
    @ApiModelProperty(value = "The codeUtepsa of the course in application", required = true)
    public String getCodeUtepsa() {
        return codeUtepsa;
    }

    public void setCodeUtepsa(String codeUtepsa) {
        this.codeUtepsa = codeUtepsa;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    @ApiModelProperty(value = "The name of the course in application", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "initials", nullable = false, length = 20)
    @ApiModelProperty(value = "The initials of the course in application", required = true)
    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        if (id != course.id) return false;
        if (name != null ? !name.equals(course.name) : course.name != null) return false;
        return initials != null ? initials.equals(course.initials) : course.initials == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (initials != null ? initials.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("course{id=%d,codeUtepsa=%s,name=%s,initials=%s}",
                this.id,this.codeUtepsa,this.name,this.initials);
    }
}
