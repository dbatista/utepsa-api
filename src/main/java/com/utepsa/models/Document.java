package com.utepsa.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by David on 22/01/2017.
 */
@Entity
@ApiModel(value = "document entity", description = "Complete info of a entity document")
public class
Document {

    private long id;
    private String codeUtepsa;
    private String description;
    private int related;

    public Document() {
    }

    public Document(long id, String codeUtepsa, String description, int related) {
        this.id = id;
        this.codeUtepsa = codeUtepsa;
        this.description = description;
        this.related = related;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "The id of the document in application", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonIgnore
    @Basic
    @Column(name = "code_utepsa", nullable = false)
    @ApiModelProperty(value = "The codeUtepsa of the document in application", required = true)
    public String getCodeUtepsa() {
        return codeUtepsa;
    }

    public void setCodeUtepsa(String codeUtepsa) {
        this.codeUtepsa = codeUtepsa;
    }

    @Basic
    @Column(name = "description", nullable = false)
    @ApiModelProperty(value = "The description of the document in application", required = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "related")
    @ApiModelProperty(value = "The related of the document in application", required = true)
    public int getRelated() {
        return related;
    }

    public void setRelated(int related) {
        this.related = related;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        if (id != document.id) return false;
        if (related != document.related) return false;
        return description != null ? description.equals(document.description) : document.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + related;
        return result;
    }

    @Override
    public String toString() {
        return String.format("document{id=%d,description=%s,related=%d}",
                this.id,this.description,this.related);
    }
}
