package com.utepsa.auth;

import com.utepsa.models.Credential;
import io.dropwizard.auth.Authorizer;

/**
 * Created by dbati on 27/7/2017.
 */
public class ApiAuthorizer implements Authorizer<Credential> {
  @Override
  public boolean authorize(Credential credential, String s) {
    return credential.getRole().getName().equals(s);
  }
}
