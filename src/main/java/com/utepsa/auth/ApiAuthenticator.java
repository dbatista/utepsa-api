package com.utepsa.auth;

import com.utepsa.db.credential.CredentialDAO;
import com.utepsa.models.Credential;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.hibernate.UnitOfWork;
import java.util.Optional;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.JwtContext;

/**
 * Created by dbati on 27/7/2017.
 */
public class ApiAuthenticator implements Authenticator<JwtContext, Credential> {

  private final CredentialDAO credentialDAO;

  public ApiAuthenticator(CredentialDAO credentialDAO) {
    this.credentialDAO = credentialDAO;
  }

  @Override
  @UnitOfWork
  public Optional<Credential> authenticate(JwtContext jwtContext) throws AuthenticationException {
    final String subject;
    try {
      subject = jwtContext.getJwtClaims().getSubject();
      return this.credentialDAO.getByUsername(subject);
    } catch (MalformedClaimException e) {
      return Optional.empty();
    }
  }
}
