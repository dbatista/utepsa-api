package com.utepsa.auth;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by dbati on 27/7/2017.
 */
public class ApiKey {
  public static byte[] getKeySHA256()
      throws NoSuchAlgorithmException, UnsupportedEncodingException {
    String secret = "apiutepsa";
    MessageDigest md = MessageDigest.getInstance("SHA-256");
    md.update(secret.getBytes("UTF-8"));
    return md.digest();
  }
}
