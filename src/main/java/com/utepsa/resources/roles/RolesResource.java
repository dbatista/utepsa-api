package com.utepsa.resources.roles;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.models.PermissionRole;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Gerardo on 14/02/2017.
 */
@Path(RolesResource.SERVICE_PATH)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "Roles", description = "Operations about Auth")
public class RolesResource {

    public static final String SERVICE_PATH = "/administrator/roles";
    @Inject
    private RolesService service;

    @GET
    @RolesAllowed({"ADMIN"})
    @Timed
    @Path("/{idRole}")
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "Authorization", value = "Authorization token [ADMIN]",
                    required = true, dataType = "string", paramType = "header")
    })
    @UnitOfWork
    @ApiOperation(value = "Returns a rol by id.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Permission Role found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "No Permission Role are available")})
    public BasicResponse getByIdRole(@ApiParam(value = "Role ID needed to find the Roles Permissions", required = true)
                                         @PathParam("idRole") long idRole) {
        return service.getByIdRole(idRole);
    }

    @PUT
    @RolesAllowed({"ADMIN"})
    @Timed
    @UnitOfWork
    @Path("/permissions")
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "Authorization", value = "Authorization token [ADMIN]",
                    required = true, dataType = "string", paramType = "header")
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Modify permissions of one Role.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "The Permission Role has been modify"),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "The Permission Role has not been modify"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse modify(
            @ApiParam(value = "JSON format is received with the Credential Administrator structure.", required = true)List<PermissionRole> permissionRoles) {
        return service.modify(permissionRoles);
    }
}
