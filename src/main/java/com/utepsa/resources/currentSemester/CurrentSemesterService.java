package com.utepsa.resources.currentSemester;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.db.currentSemester.CurrentSemesterDAO;
import com.utepsa.models.CurrentSemester;

/**
 * Created by Luana Chavez on 13/03/2017.
 */
public class CurrentSemesterService {
    private final CurrentSemesterDAO currentSemesterDAO;

    @Inject
    public CurrentSemesterService(CurrentSemesterDAO currentSemesterDAO) {
        this.currentSemesterDAO = currentSemesterDAO;
    }

    public BasicResponse getCurrentSemester()
    {
        BasicResponse basicResponse =new BasicResponse();
        try{
            CurrentSemester currentSemester = currentSemesterDAO.getCurrentSemester();
            if (currentSemester !=null)
            {
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(currentSemester);
            }else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Current Semester not found");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }
}
