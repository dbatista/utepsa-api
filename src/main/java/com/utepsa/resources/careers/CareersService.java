package com.utepsa.resources.careers;

import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.Operations;
import com.utepsa.core.StatusCode;
import com.utepsa.models.Career;
import com.utepsa.db.career.CareerDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by David on 08/11/2016.
 */
public class CareersService {
    private final CareerDAO careerDAO;

    private Function<String, Boolean> existGrade = g -> {
        if(g.toUpperCase().equals("PRE") || g.toUpperCase().equals("POST")) return true;
        return false;
    };

    @Inject
    public CareersService(CareerDAO careerDAO) {
        this.careerDAO = careerDAO;
    }

    public BasicResponse findByGrade(String grade){
        BasicResponse basicResponse = new BasicResponse();
        try{
            if(!existGrade.apply(grade)){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("this grade not found");
                return basicResponse;
            }
            List<Career> careers = careerDAO.getByGrade(grade);
            if(careers == null || careers.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(careers);
            }
        }catch (Exception e){
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getAll(){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<Career> careers = careerDAO.getAll();
            if(careers.size() == 0){
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("There are no careers to show");
            }else{
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(careers);
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }


}
