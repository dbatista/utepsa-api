package com.utepsa.resources.careers;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.core.StatusCode;
import com.utepsa.models.Career;
import com.utepsa.api.response.BasicResponse;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
/**
 * Created by David on 08/11/2016.
 */
@Path(CareersResource.SERVICE_PATH)
@Api(value = "Careers", description = "Operations about Careers")
@Produces(MediaType.APPLICATION_JSON)
public class CareersResource {
    public static final String SERVICE_PATH = "careers/";

    @Inject
    private CareersService service;

    public CareersResource() {
    }

    @GET
    @RolesAllowed({"STUDENT", "ADMIN"})
    @Timed
    @UnitOfWork
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "Authorization", value = "Authorization token [STUDENT, ADMIN]",
                    required = true, dataType = "string", paramType = "header")
    })
    @ApiOperation( value = "Get all Careers", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Careers found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no careers to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse getAll() {
        return service.getAll();
    }

    @GET
    @RolesAllowed({"STUDENT", "ADMIN"})
    @Timed
    @UnitOfWork
    @Path("grade/{grade}")
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "Authorization", value = "Authorization token [STUDENT, ADMIN]",
                    required = true, dataType = "string", paramType = "header")
    })
    @ApiOperation( value = "Find a Careers By Grade (Postgraduate or Undergraduate)", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Careers found"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no careers to show"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse findByCareerCode(@ApiParam(value = "Grade to search", required = true)
                                              @PathParam("grade") String grade){
        return service.findByGrade(grade);
    }

}
