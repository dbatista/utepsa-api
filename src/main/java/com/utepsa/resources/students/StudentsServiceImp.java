package com.utepsa.resources.students;

import com.google.inject.Inject;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentAdapter;
import com.utepsa.adapters.utepsa.documentsStudent.DocumentsStudentData;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesAdapter;
import com.utepsa.adapters.utepsa.historyNotes.HistoryNotesData;
import com.utepsa.adapters.utepsa.students.StudentAdapter;
import com.utepsa.adapters.utepsa.students.StudentData;
import com.utepsa.api.custom.HistoryNotesCustom;
import com.utepsa.api.custom.StudentHistoryNotes;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.config.ExternalServer;
import com.utepsa.core.GlobalConfig;
import com.utepsa.core.StatusCode;
import com.utepsa.db.careerCourse.CareerCourseDAO;
import com.utepsa.db.course.CourseDAO;
import com.utepsa.db.documentStudent.DocumentStudentDAO;
import com.utepsa.db.historyNote.HistoryNotesDAO;
import com.utepsa.db.notification.NotificationDAO;
import com.utepsa.db.student.StudentDAO;
import com.utepsa.models.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by roberto on 12/7/2016.
 */
public class StudentsServiceImp {

    private final StudentDAO dao;
    private final HistoryNotesDAO historyNotesDao;
    private final CareerCourseDAO careerCourseDAO;
    private final NotificationDAO notificationDAO;
    private final DocumentStudentDAO documentStudentDAO;
    private final HistoryNotesAdapter historyNotesAdapter;
    private final DocumentsStudentAdapter documentsStudentAdapter;
    private final StudentAdapter studentAdapter;
    private final CourseDAO courseDAO;

    @Inject
    public StudentsServiceImp(HistoryNotesAdapter historyNotesAdapter, DocumentsStudentAdapter documentsStudentAdapter,
                              StudentAdapter studentAdapter, StudentDAO dao, HistoryNotesDAO historyNotesDao,
                              CareerCourseDAO careerCourseDAO, NotificationDAO notificationDAO, DocumentStudentDAO documentStudentDAO,
                              CourseDAO courseDAO) {
        this.dao = dao;
        this.historyNotesDao = historyNotesDao;
        this.careerCourseDAO = careerCourseDAO;
        this.notificationDAO = notificationDAO;
        this.documentStudentDAO = documentStudentDAO;
        this.historyNotesAdapter = historyNotesAdapter;
        this.documentsStudentAdapter = documentsStudentAdapter;
        this.studentAdapter = studentAdapter;
        this.courseDAO = courseDAO;
    }

    public BasicResponse getById (long idStudent) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getById(idStudent);
            if(student != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(student);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student not found");
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getNotificationsByStudent(long idStudent, int startRow) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getById(idStudent);
            if(student != null){
                List<Notification> notifications = notificationDAO.getByStudent(student.getRegisterNumber(),student.getCareer().getId(), GlobalConfig.NOTIFICATIONS_PER_STUDENT,startRow);
                if(notifications != null && notifications.size() > 0){
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setData(notifications);
                }else{
                    basicResponse.setCode(StatusCode.NO_CONTENT);
                    basicResponse.setMessage("There are no notifications to display");
                }
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student not found");
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getDocumentsByStudent(long idStudent){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getById(idStudent);
            if(student != null){
                List<DocumentStudent> documents = documentStudentDAO.getByStudent(idStudent);
                if(documents != null && documents.size() > 0){
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setData(documents);
                }else{
                    basicResponse.setCode(StatusCode.NO_CONTENT);
                    basicResponse.setMessage("This student does not have any documents registered");
                }
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student not found");
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse getHistoryNotesByStudent(long idStudent){
        BasicResponse basicResponse = new BasicResponse();
        try{
            Student student = dao.getById(idStudent);
            List<HistoryNotes> historyNotes = historyNotesDao.getByStudent(idStudent);
            if (historyNotes!=null)
            {
                List<CareerCourse> coursesByCareer = careerCourseDAO.getCoursesByCareer(student.getCareer().getId(), student.getPensum());
                List<Course> coursesMissing = historyNotesDao.getCoursesMissingByStudent(idStudent, student.getCareer().getId(), student.getPensum());
                List<Double> summary = getStudentSummary(historyNotes,null);

                double progressPercentage = (summary.get(0) * 100) / (double) coursesByCareer.size();

                StudentHistoryNotes studentHistoryNotes = new StudentHistoryNotes();
                studentHistoryNotes.setApproved(summary.get(0).intValue());
                studentHistoryNotes.setDisapproved(summary.get(1).intValue());
                studentHistoryNotes.setAverage(summary.get(2));
                studentHistoryNotes.setTotal(coursesByCareer.size());
                studentHistoryNotes.setProgressPercentage(progressPercentage);
                studentHistoryNotes.setCourses(historyNotes);
                studentHistoryNotes.setCoursesMissing(coursesMissing);

                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(studentHistoryNotes);
                return basicResponse;
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    private List<Double> getStudentSummary(List<HistoryNotes> courses,List<HistoryNotesCustom> coursesCustom){
        List<Double> summary = new ArrayList<>();
        double approved = 0;
        double disapproved = 0;
        double note = 0;
        double average = 0;
        double total = 0;

        if (courses!=null)
        {
            for(HistoryNotes course: courses){
                total++;
                note = note + course.getNote();
                if(course.getNote() >= course.getMinimunNote()){
                    approved++;
                }else{
                    disapproved++;
                }
            }
        }
        else{
            for(HistoryNotesCustom courseCustom: coursesCustom){
                if(courseCustom.getIdStudent() == null) continue;
                total++;
                note = note + courseCustom.getNote();
                if(courseCustom.getNote() >= courseCustom.getMinimunNote()){
                    approved++;
                }else{
                    disapproved++;
                }
            }
        }
        average = note / total;
        summary.add(approved);
        summary.add(disapproved);
        summary.add(average);

        return summary;
    }


    public BasicResponse getStudentByRegisterNumber(String registerNumber){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Student student = dao.getByRegisterNumber(registerNumber);
            if(student != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(student);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("Student not found");
            }
        } catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public BasicResponse updateStudentData(String registerCode)
    {
        BasicResponse basicResponse = new BasicResponse();
        try {
            StudentData studentData = studentAdapter.getStudent(registerCode);
            Student student = dao.getByRegisterNumber(registerCode);
            if (studentData !=null && student !=null)
            {
                student.setName(studentData.getAgd_nombres().trim());
                student.setFatherLastname(studentData.getAgd_appaterno().trim());
                student.setMotherLastname(studentData.getAgd_apmaterno().trim());
                student.setPhoneNumber1(studentData.getAgd_telf1().trim());
                student.setPhoneNumber2(studentData.getAgd_telf2().trim());
                student.setEmail1(studentData.getCorreo().trim());
                syncHistoryNotes(student);
                syncDocuments(student);
            }
            else
            {
                basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
                basicResponse.setMessage("This data has no Student to display");
            }
        }catch (Exception e){
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }

    public void syncHistoryNotes(Student student) throws Exception {
        List<HistoryNotesData> historyNotesUtepsa = historyNotesAdapter.getAllHistoryNotesStudent(student.getRegisterNumber());
        List<HistoryNotes> historyNotesApp = historyNotesDao.getByStudent(student.getId());

        if(historyNotesUtepsa.size() <= 0) return;

        for(HistoryNotesData data : historyNotesUtepsa){
            Course course = courseDAO.getByCodeUtepsa(data.getMat_codigo().trim());
            for(HistoryNotes historyNote: historyNotesApp){
                if(historyNote.getSemester().equals(data.getSem_codigo()) && historyNote.getModule().equals(data.getMdu_codigo()) && historyNote.getCourse().getId() == course.getId()){
                    if(historyNote.getNote() != data.getNot_nota()){
                        historyNote.setNote((int) data.getNot_nota());
                    }
                    return;
                }
            }
        }
    }

    public void syncDocuments(Student student) throws Exception {
        List<DocumentStudent> documentsStudentApp = documentStudentDAO.getByStudent(student.getId());
        List<DocumentsStudentData> documentsStudentUtepsa = documentsStudentAdapter.getAllDocumentsStudentForStudent(student.getRegisterNumber());

        if(documentsStudentUtepsa.size() <= 0) return;

        for(DocumentsStudentData data: documentsStudentUtepsa){
            for(DocumentStudent documentStudent: documentsStudentApp){
                    if(!documentStudent.getTypePaper().equals(data.getAlmdoc_tipopapel().trim())){
                        documentStudent.setTypePaper(data.getAlmdoc_tipopapel().trim());
                    }


                    SimpleDateFormat formaterTimeZone = new SimpleDateFormat("yyyy-MM-dd");
                    if(data.getAlmdoc_fechaentregar() != null){
                        Date dateDelivery = formaterTimeZone.parse(data.getAlmdoc_fechaentregar().trim());

                        if(documentStudent.getDateDelivery() != dateDelivery){
                            documentStudent.setDateDelivery(dateDelivery);
                        }
                    }

                    if(data.getAlmdoc_fecharecepcion() != null){
                        Date dateReceipt = formaterTimeZone.parse(data.getAlmdoc_fecharecepcion().trim());
                        if(documentStudent.getDateReceipt() != dateReceipt){
                            documentStudent.setDateReceipt(dateReceipt);
                        }
                    }

                    boolean state;
                    if(data.getESTADO().trim().contains("ENTREGADO")){
                        state = true;
                    }else{
                        state = false;
                    }

                    if(documentStudent.isState() != state){
                        documentStudent.setState(state);
                    }
                    return;
                }
        }
    }

    public BasicResponse getByFullName(String fullName,String registerNumber)
    {
        BasicResponse basicResponse = new BasicResponse();
        try {
            if (fullName !=null && registerNumber != null)
            {
                basicResponse.setCode(StatusCode.CONFLICT);
                basicResponse.setMessage("The search can only be done by a parameter");
                return basicResponse;
            }
            if (fullName ==null && registerNumber == null)
            {
                basicResponse.setCode(StatusCode.CONFLICT);
                basicResponse.setMessage("Please enter a parameter");
                return basicResponse;
            }
            List<Student> students;
            if(fullName !=null && registerNumber == null){
                students = dao.getByFullName(fullName.toUpperCase());
            }
            else{
                return getStudentByRegisterNumber(registerNumber.toUpperCase());
            }
            if (students.size()>0)
            {
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setData(students);
            }else {
                basicResponse.setCode(StatusCode.NO_CONTENT);
                basicResponse.setMessage("Student does't exist");
            }
        }catch (Exception e) {
            basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            basicResponse.setMessage(e.getCause().getMessage());
        }
        return basicResponse;
    }
}
