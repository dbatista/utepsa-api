package com.utepsa.resources.students;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.models.HistoryNotes;
import com.utepsa.models.Student;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by roberto on 12/7/2016.
 */
@Path(StudentsResource.SERVICE_PATH)
@Api(value = "Student", description = "Operations about Students")
@Produces(MediaType.APPLICATION_JSON)
public class StudentsResource {
    public static final String SERVICE_PATH = "student/";
    @Inject
    private StudentsServiceImp service;

    @GET
    @RolesAllowed({"STUDENT", "ADMIN"})
    @Timed
    @UnitOfWork
    @Path("/{idStudent}")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiImplicitParams( {
        @ApiImplicitParam(name = "Authorization", value = "Authorization token [STUDENT, ADMIN]",
            required = true, dataType = "string", paramType = "header")
    })
    @ApiOperation( value = "Get Student by id", response = Student.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getStudentById(@ApiParam(value = "Student ID needed to find the " +
                                            "student", required = true)
                                            @PathParam("idStudent") long idStudent) {
            return service.getById(idStudent);
    }

    @GET
    @RolesAllowed({"STUDENT", "ADMIN"})
    @Timed
    @UnitOfWork
    @Path("/{idStudent}/notifications/{numberRow}")
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "Authorization", value = "Authorization token [STUDENT, ADMIN]",
                    required = true, dataType = "string", paramType = "header")
    })
    @ApiOperation( value = "Gets notifications of a Student by id", response = Student.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Notifications obtained correctly"),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "There are no notifications to display"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getNotificationsByStudent(
            @ApiParam(value = "Student ID needed to find the student", required = true)
            @PathParam("idStudent") long idStudent,
            @ApiParam(value = "Row number to get", required = true)
            @PathParam("numberRow") int numRow) {
        return service.getNotificationsByStudent(idStudent, numRow);
    }

    @GET
    @RolesAllowed({"STUDENT", "ADMIN"})
    @Timed
    @UnitOfWork
    @Path("{idStudent}/historyNotes")
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "Authorization", value = "Authorization token [STUDENT, ADMIN]",
                    required = true, dataType = "string", paramType = "header")
    })
    @ApiOperation( value = "Get the History Notes of a Student by id and chronological order or pensum order", response = HistoryNotes.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student history notes were successfully obtained."),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getHistoryNotesByStudentChronological(@ApiParam(value = "Student ID needed to find the student", required = true)
                                                      @PathParam("idStudent") long idStudent){
        return service.getHistoryNotesByStudent(idStudent);
    }

    @GET
    @RolesAllowed({"STUDENT", "ADMIN"})
    @Timed
    @UnitOfWork
    @Path("/{idStudent}/documents")
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "Authorization", value = "Authorization token [STUDENT, ADMIN]",
                    required = true, dataType = "string", paramType = "header")
    })
    @ApiOperation( value = "Find DocumentsStudent by register code", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student documents were successfully obtained."),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "This student does not have any documents registered"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "This order doesn't exist, please select: C or P"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getDocumentsStudent (@ApiParam(value = "Student ID needed to find the student", required = true)
                                                  @PathParam("idStudent") long idStudent) {
            return service.getDocumentsByStudent(idStudent);
    }

    /*@GET
    @Timed
    @UnitOfWork
    @Path("/search/")
    @ApiOperation( value = "Get student by register number or full name", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Student files were successfully obtained."),
            @ApiResponse(code = StatusCode.NO_CONTENT, message = "Student does't exist"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "The search can only be done by a parameter"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse searchStudent (@ApiParam(value = "registerNumber of Student that needs to be fetched by register")
                                        @QueryParam("registerNumber") String registerNumber,
                                        @ApiParam(value = "Full Name of Student that needs to be fetched by name")
                                        @QueryParam("fullName") String fullName) {
        return service.getByFullName(fullName,registerNumber);
    }*/

    /*@PUT
    @Timed
    @UnitOfWork
    @Path("{registerCode}/sync")
    @ApiOperation( value = "sync data by student", response = BasicResponse.class)
    @ApiResponses(value = {
        @ApiResponse(code = StatusCode.OK, message = "Student data updated."),
        @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "This data has no Student to display"),
        @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse  updateStudentData(@ApiParam(value = "Register code of Student that needs to be fetched", required = true)
        @PathParam("registerCode") String registerCode) {
            return service.updateStudentData(registerCode.toUpperCase());
    }*/
}
