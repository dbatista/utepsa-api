package com.utepsa.resources.notifications;


import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.utepsa.api.response.BasicResponse;
import com.utepsa.core.StatusCode;
import com.utepsa.models.Notification;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import io.swagger.annotations.*;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by DAvid on 22/07/2016.
 */
@Path(NotificationsResource.SERVICE_PATH)
@Api(value = "Notifications", description = "Operations about Notifications")
@Produces(MediaType.APPLICATION_JSON)
public class NotificationsResource {

    public static final String SERVICE_PATH = "notifications/";
    @Inject
    private NotificationService service;

    @POST
    @RolesAllowed({"ADMIN"})
    @Timed
    @UnitOfWork
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "Authorization", value = "Authorization token [ADMIN]",
                    required = true, dataType = "string", paramType = "header")
    })
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Create a Notification", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Notification created correctly"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Student not found"),
            @ApiResponse(code = StatusCode.CONFLICT, message = "Service GCM not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse createNotification(@ApiParam(value = "JSON format is received with the notification structure.", required = true) Notification notification) {
        return service.createNotification(notification);
    }

    @GET
    @RolesAllowed({"ADMIN"})
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "Authorization", value = "Authorization token [ADMIN]",
                    required = true, dataType = "string", paramType = "header")
    })
    @ApiOperation( value = "Get Notification by ID", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Notification obtained correctly"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Notification not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse getNotificationById(@ApiParam(value = "Notification ID needed to find the notification", required = true)
                                                 @PathParam("id") LongParam id) {
        try {
            return service.getNotificationById(id.get());
        } catch (NotFoundException exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GET
    @RolesAllowed({"ADMIN"})
    @Timed
    @UnitOfWork
    @ApiImplicitParams( {
            @ApiImplicitParam(name = "Authorization", value = "Authorization token [ADMIN]",
                    required = true, dataType = "string", paramType = "header")
    })
    @ApiOperation( value = "Get all Notification", response = Notification.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Notification obtained correctly"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Notification not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse getAllNotifications() {
        try {
            return service.getAllNotifications();
        } catch (NotFoundException exception) {
            throw new WebApplicationException(exception.getMessage(), Response.Status.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}