FROM java:8

ADD ./target/notificationserver-1.0-SNAPSHOT.jar /app/produccion/utepsaapp.jar

ADD config-remote.yml /app/produccion/config.yml

CMD java -jar /app/produccion/utepsaapp.jar server /app/produccion/config.yml

EXPOSE 9090 9091
